from .token import Token
from .sentence import Sentence
from pathlib import Path

# paths for the english data #####################################

train_path_eng = Path.cwd() / 'data' / 'english' / 'train'
train_file_10_eng = train_path_eng / 'wsj_train.first-10.conll06'
train_file_1h_eng = train_path_eng / 'wsj_train.first-1h.conll06'
train_file_1k_eng = train_path_eng / 'wsj_train.first-1k.conll06'
train_file_5k_eng = train_path_eng / 'wsj_train.first-5k.conll06'
train_file_eng = train_path_eng / 'wsj_train.conll06'

dev_path_eng = Path.cwd() / 'data' / 'english' / 'dev'
dev_file_1_eng = dev_path_eng / 'wsj_dev.short-1.conll06.gold'
dev_file_1h_eng = dev_path_eng / 'wsj_dev.first-100.conll06.gold'
dev_file_eng = dev_path_eng / 'wsj_dev.conll06.gold'

test_path_eng = Path.cwd() / 'data' / 'english' / 'test'
test_file_eng = test_path_eng / 'wsj_test.conll06.blind'
test_file_out_eng = test_path_eng / 'wsj_test.conll06.pred'

# paths for the german data #####################################

train_path_ger = Path.cwd() / 'data' / 'german' / 'train'
train_file_1h_ger = train_path_ger / 'tiger-2.2.train.first-1h.conll06'
train_file_1k_ger = train_path_ger / 'tiger-2.2.train.first-1k.conll06'
train_file_5k_ger = train_path_ger / 'tiger-2.2.train.first-5k.conll06'
train_file_ger = train_path_ger / 'tiger-2.2.train.conll06'

dev_path_ger = Path.cwd() / 'data' / 'german' / 'dev'
dev_file_ger = dev_path_ger / 'tiger-2.2.dev.conll06.gold'
dev_file_1h_ger = dev_path_ger / 'tiger-2.2.dev.first-1h.conll06.gold'


test_path_ger = Path.cwd() / 'data' / 'german' / 'test'
test_file_ger = test_path_ger / 'tiger-2.2.test.conll06.blind'
test_file_out_ger = test_path_ger / 'tiger-2.2.test.conll06.pred'


def count_sentences(file_name, type):
    length = len(read_conll_file(file_name, type))
    return length


def read_conll_file(file_name, type):
    sentence_list = []
    sent_id = 0
    with open(file_name) as f:
        one_sentence = []
        # adds a special token root to each sentence
        # id, form, lemma, pos, xpos, morph, head, rel
        root_token = Token(0, "root", "root", "ROOT", "_", "_", "_", "_")
        one_sentence.append(root_token)
        for line in f:
            # end/begin of sentence
            if line in ('\n', '\r\n'):
                new_sentence = Sentence(one_sentence, id=sent_id)
                sent_id += 1
                sentence_list.append(new_sentence)
                one_sentence = []
                one_sentence.append(root_token)
            else:
                splitted = str.split(line, "\t")
                if (type == "pred"):
                    new_token = Token(id=int(splitted[0]), form=splitted[1], lemma=splitted[2], pos=splitted[3],
                                      xpos=splitted[4], morph=splitted[5], pred_head=int(splitted[6]), pred_rel=splitted[7])
                    one_sentence.append(new_token)
                elif (type == "gold"):
                    new_token = Token(id=int(splitted[0]), form=splitted[1], lemma=splitted[2], pos=splitted[3],
                                      xpos=splitted[4], morph=splitted[5], gold_head=int(splitted[6]), gold_rel=splitted[7])
                    one_sentence.append(new_token)
                elif (type == "blind"): # no gold head or relation
                    new_token = Token(id=int(splitted[0]), form=splitted[1], lemma=splitted[2], pos=splitted[3],
                                      xpos=splitted[4], morph=splitted[5], gold_head=splitted[6], gold_rel=splitted[7])
                    one_sentence.append(new_token)
        # don't take a list with just a "root" entry
        if (len(one_sentence) > 1):
            sentence_list.append(one_sentence)
    return sentence_list


def write_pred_conll_file(fileName, sentence_list):
    f = open(fileName, "w")
    for sentence in sentence_list:
        # skip first token of each sentence because it's just a "root" entry
        for each_token in sentence.token_list[1:]:
            f.write(str(each_token.id) + "\t" + each_token.form + "\t" + each_token.lemma + "\t" + each_token.pos + "\t" +
                    each_token.xpos + "\t" + each_token.morph + "\t" + str(each_token.pred_head) + "\t" + each_token.pred_rel + "\t_\t_\n")
        f.write("\n")




