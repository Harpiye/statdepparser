import gzip
import pickle
from pathlib import Path

model_path = Path().cwd() / 'data' / 'output' / 'model'
pred_path = Path().cwd() / 'data' / 'output' / 'pred'

def save_data(file_name, data):
    file_path = model_path / file_name
    stream = gzip.open(file_path, 'wb')
    pickle.dump(data, stream, -1)
    stream.close()


def load_data(file_name):
    file_path = model_path / file_name
    stream = gzip.open(file_path, 'rb')
    data = pickle.load(stream)
    stream.close()
    return data


def save_weights(file_name, weights):
    # TODO: check if it overwrites existing files (it should in my case)
    file_path = model_path / file_name
    stream = gzip.open(file_path, 'wb')
    pickle.dump(weights, stream, -1)
    stream.close()


def load_weights(file_name):
    file_path = model_path / file_name
    stream = gzip.open(file_path, 'rb')
    weights = pickle.load(stream)
    stream.close()
    return weights