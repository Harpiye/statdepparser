
class Token:
    # initializing the variables: correspond to the columns in the conll format
    id = 0
    form = ""
    lemma = ""
    pos = ""
    xpos = ""
    morph = ""
    gold_head = 0
    gold_rel = ""
    pred_head = 0
    pred_rel = ""


    def __init__(self, id, form, lemma, pos, xpos, morph, gold_head=None, gold_rel=None, pred_head=None, pred_rel=None):
        self.id = id # 0
        self.form = form # 1
        self.lemma = lemma # 2
        self.pos = pos # 3
        self.xpos = xpos # 4
        self.morph = morph # 5
        self.gold_head = 0 if gold_head is None else gold_head # 6
        self.gold_rel = "" if gold_rel is None else gold_rel # 7
        self.pred_head = 0 if pred_head is None else pred_head # 8
        self.pred_rel = "" if pred_rel is None else pred_rel # 9