from graphs import graph_structure

class Sentence:

    def __init__(self, token_list, id=None, full_graph=None, final_graph=None, gold_graph=None):
        self.token_list = token_list
        self.id = -1 if id is None else id
        self.full_graph = graph_structure.Graph() if full_graph is None else full_graph # fully-connected graph
        self.pred_graph = graph_structure.Graph() if final_graph is None else final_graph # predicted graph
        self.gold_graph = graph_structure.Graph() if gold_graph is None else gold_graph # gold graph
