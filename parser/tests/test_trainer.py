import unittest
from pathlib import Path
from graphs import graph_structure
from machine_learning import feature_extractor as fe
from reader_and_writer import reader_and_writer as raw
from reader_and_writer.saver_and_loader import *
from machine_learning.perceptron.structured_perceptron import Perceptron


class test_trainer(unittest.TestCase):
    def english_short_test_pipeline(self):
        file_prefix = "test"
        # read necessary files
        sentence_list_train = raw.read_conll_file(raw.train_file_10_eng, "gold")
        sentence_list_dev = raw.read_conll_file(raw.dev_file_1_eng, "gold")

        # create all fully connected graphs
        graph_structure.create_all_full_graphs(sentence_list_train)
        graph_structure.create_all_full_graphs(sentence_list_dev)

        # create all gold graphs
        graph_structure.create_all_gold_graphs(sentence_list_train)
        graph_structure.create_all_gold_graphs(sentence_list_dev)

        # extract features for training set
        feat_extractor = fe.Feature_extractor()
        print("Extracting features for training set...")
        feat_extractor.extract_all_features(sentence_list_train, False)
        print(str(feat_extractor.string_mapper.feat_counter - 1) + " unique features extracted.")

        # set freeze flag to indicate that no new features can be added anymore
        feat_extractor.string_mapper.freeze = True
        # extract features for development and test set
        print("Extracting features for development and test set...")
        feat_extractor.extract_all_features(sentence_list_dev, False)
        # feat_extractor.extract_all_features(sentence_list_test)

        perceptron = Perceptron(feat_extractor.string_mapper.feat_counter)
        perceptron.train(sentence_list_train, sentence_list_dev, None, file_prefix, 100, shuffle_flag=False)