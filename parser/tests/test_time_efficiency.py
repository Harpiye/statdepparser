import timeit
import unittest

class test_time_efficiency(unittest.TestCase):

    def test_time_efficiency(self):
        # code snippet to be executed only once
        test_setup = "from decoding import chu_liu_edmonds as cle;from graphs import exercise_graph, graph_structure;" \
                     "from pathlib import Path;from reader_and_writer import reader_and_writer as raw;sentence_list_train=" \
                     "raw.read_conll_file(Path().resolve().parent/'data'/'english'/'train'/'wsj_train.conll06','gold');" \
                     "sent1 = sentence_list_train[1198];sent2 = sentence_list_train[13606]"

        nr = 10 # 1000000 #default

        time1 = timeit.timeit(stmt='graph1=graph_structure.create_graph_with_all_arcs_randomly(sent1);'
                                   'cle.chu_liu_edmonds_algorithm(graph1)', setup=test_setup, number=nr)
        print("CLE for a sentence with 72 tokens takes ", time1, " seconds.")
        time2 = timeit.timeit(stmt='graph2 =graph_structure.create_graph_with_all_arcs_randomly(sent2);'
                                   'cle.chu_liu_edmonds_algorithm(graph2)', setup=test_setup, number=nr)
        print("CLE for a sentence with 144 tokens takes ", time2, " seconds.")
        ratio = time2 / time1
        print("That's ", ratio, " times as long.")
        self.assertLessEqual(ratio, 8)

