import unittest
from pathlib import Path
from reader_and_writer import reader_and_writer as raw
from evaluation import attachment_score as eval



class test_chu_liu_edmonds(unittest.TestCase):
    def test_evaluate_dev_set(self):
        dev_path = Path().resolve().parent / 'data' / 'english' / 'dev'
        pred_file = 'wsj_dev.conll06.pred'
        gold_file = 'wsj_dev.conll06.gold'
        sentence_list_pred = raw.read_conll_file(dev_path / pred_file, "pred")
        sentence_list_gold = raw. read_conll_file(dev_path / gold_file, "gold")
        results = eval.evaluate(sentence_list_pred, sentence_list_gold)
        self.assertEqual(26595, results[0], "The number of tokens should be 26595 but is " + str(results[0]) + "!")
        self.assertEqual(23867, results[1], "The number of correct unlabelled heads should be 23867 but is " + str(results[1]) + "!")
        self.assertEqual(89.74, results[2], "UAS should be 89.74% but is " + str(results[2]) + "!")
        self.assertEqual(23434, results[3], "The number of correct labelled heads should be 23434 but is " + str(results[3]) + "!")
        self.assertEqual(88.11, results[4], "LAS should be 88.11% but is " + str(results[4]) + "!")