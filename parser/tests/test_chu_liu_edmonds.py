import unittest

from decoding import chu_liu_edmonds as cle
from pathlib import Path
from graphs import exercise_graph, graph_structure
from reader_and_writer import reader_and_writer as raw
from evaluation import attachment_score as eval
from graphs import drawing
import timeit


class test_chu_liu_edmonds(unittest.TestCase):
    def test_chu_liu_edmonds_algorithm(self):
        """
        Tests whether the cle algorithm returns the correct graphs for examples from the class.

        :return:
        """
        # "John saw Mary"
        test_graph_1 = exercise_graph.create_ex_graph_1()
        final_graph_1 = exercise_graph.create_final_ex_graph_1()

        test_graph_2 = exercise_graph.create_ex_graph_2()
        final_graph_2 = exercise_graph.create_final_ex_graph_2()

        # "read a book"
        test_graph_3 = exercise_graph.create_ex_graph_3()
        final_graph_3 = exercise_graph.create_final_ex_graph_3()

        # "I read a book"; containing a cycle consisting of three nodes
        test_graph_result_1 = cle.chu_liu_edmonds_algorithm(test_graph_1)
        test_graph_result_2 = cle.chu_liu_edmonds_algorithm(test_graph_2)
        test_graph_result_3 = cle.chu_liu_edmonds_algorithm(test_graph_3)

        self.assertEqual(final_graph_1.arcs_dict, test_graph_result_1.arcs_dict, ("The arcs_dict should be ",
                         final_graph_1.arcs_dict, " but is ", test_graph_result_1.arcs_dict, "!"))
        self.assertEqual(final_graph_2.arcs_dict, test_graph_result_2.arcs_dict, ("The arcs_dict should be ",
                         final_graph_2.arcs_dict, " but is ", test_graph_result_2.arcs_dict, "!"))
        self.assertEqual(final_graph_3.arcs_dict, test_graph_result_3.arcs_dict, ("The arcs_dict should be ",
                         final_graph_3.arcs_dict, " but is ", test_graph_result_3.arcs_dict, "!"))


    def test_chu_liu_edmonds_algorithm2(self):
        """
        Tests whether the cle algorithm returns the correct graphs for an example graph.

        :return:
        """
        test_graph_4 = exercise_graph.create_ex_graph_4()
        final_graph_4 = exercise_graph.create_final_ex_graph_4()

        test_graph_result_4 = cle.chu_liu_edmonds_algorithm(test_graph_4)

        self.assertEqual(final_graph_4.arcs_dict, test_graph_result_4.arcs_dict, ("The arcs_dict should be ",
                         final_graph_4.arcs_dict, " but is ", test_graph_result_4.arcs_dict, "!"))


    def test_chu_liu_edmonds_algorithm3(self):
        """
        Tests whether the cle algorithm returns the correct graphs for an example graph.

        :return:
        """
        g = graph_structure.create_graph_from_dict({0: {1: -2, 2: 3, 3: -3, 4: 0, 5: -4, 6: -31}, 1: {2: 14, 3: 10, 4: 4, 5: 5, 6: -2},
                                                    2: {1: 8, 3: 42, 4: 0, 5: -1, 6: -6}, 3: {1: -3, 2: -1, 4: 8, 5: 3, 6: 3}, 4: {1: 1, 2: 0, 3: 4, 5: 9, 6: -1},
                                                    5: {1: -1, 2: 3, 3: 7, 4: 26, 6: -3}, 6: {1: -8, 2: -2, 3: -4, 4: -6, 5: -8}})

        test_graph_result_4 = cle.chu_liu_edmonds_algorithm(g)
        final_graph_4 = graph_structure.create_graph_from_dict({0: {1: -2}, 1: {5: 5, 2: 14}, 2: {3: 42}, 3: {6: 3}, 4: {}, 5: {4: 26}, 6: {}})
        #drawing.draw_graph(test_graph_result_4)


        self.assertEqual(final_graph_4.arcs_dict, test_graph_result_4.arcs_dict, ("The arcs_dict should be ",
                                                                                  final_graph_4.arcs_dict,
                                                                                   " but is ",
                                                                                   test_graph_result_4.arcs_dict,
                                                                                   "!"))

    def test_cle_for_first_1k_for_correctness(self):
        train_path = Path().resolve().parent / 'data' / 'english' / 'train'
        train_file = 'wsj_train.first-1k.conll06'
        #train_file = 'wsj_train.conll06'

        sentence_list_1k = raw.read_conll_file(train_path / train_file, "gold")
        for each_sent in sentence_list_1k:
            each_sent.full_graph = graph_structure.create_graph_with_all_arcs_correctly(each_sent)
            each_sent.pred_graph = cle.chu_liu_edmonds_algorithm(each_sent.full_graph)
            for token in range(1, len(each_sent.token_list)):
                each_sent.token_list[token].pred_head = list(each_sent.pred_graph.nodes_dict[token].heads.keys())[0]

        results = eval.evaluate_list(sentence_list_1k)
        # return token_count, correct_head_count, UAS,
        self.assertEqual(25398, results[0]) # number of tokens without 0 / non-empty lines
        self.assertEqual(25398, results[1]) # correct_head_count
        self.assertEqual(100.0, results[2]) # UAS
        #eval.check_sentence(each_sent)
        #raw.write_pred_conll_file(train_path / 'wsj_train.first-1k.conll06.pred', sentence_list_1k)


    def test_cle_for_first_1k_for_functionality(self):
        train_path = Path().resolve().parent / 'data' / 'english' / 'train'
        train_file = 'wsj_train.first-1k.conll06'

        sentence_list_1k = raw.read_conll_file(train_path / train_file, "gold")
        for each_sent in sentence_list_1k:
            each_sent.full_graph = graph_structure.create_graph_with_all_arcs_randomly(each_sent)
            each_sent.pred_graph = cle.chu_liu_edmonds_algorithm(each_sent.full_graph)
            for token in range(1, len(each_sent.token_list)):
                each_sent.token_list[token].pred_head = list(each_sent.pred_graph.nodes_dict[token].heads.keys())[0]
        results = eval.evaluate_list(sentence_list_1k)
        #eval.check_sentence(each_sent)
        #raw.write_pred_conll_file(train_path / 'wsj_train.first-1k.conll06.pred', sentence_list_1k)


    def test_cle_for_dev_for_correctness(self):
        dev_path = Path().resolve().parent / 'data' / 'english' / 'dev'
        dev_file = 'wsj_dev.conll06.gold'

        sentence_list = raw.read_conll_file(dev_path / dev_file, "gold")
        for each_sent in sentence_list:
            each_sent.full_graph = graph_structure.create_graph_with_all_arcs_correctly(each_sent)
            each_sent.pred_graph = cle.chu_liu_edmonds_algorithm(each_sent.full_graph)
            for token in range(1, len(each_sent.token_list)):
                each_sent.token_list[token].pred_head = list(each_sent.pred_graph.nodes_dict[token].heads.keys())[0]
        results = eval.evaluate_list(sentence_list)
        # return token_count, correct_head_count, UAS,
        self.assertEqual(26595, results[0]) # number of tokens without 0 / non-empty lines
        self.assertEqual(26595, results[1]) # correct_head_count
        self.assertEqual(100.0, results[2]) # UAS
        #eval.check_sentence(each_sent)
        #raw.write_pred_conll_file(train_path / 'wsj_train.first-1k.conll06.pred', sentence_list_1k)


    def test_dev_example(self):
        dev_path = Path().resolve().parent / 'data' / 'english' / 'dev'
        dev_file = 'wsj_dev.short-1.conll06.gold'

        sentence_list = raw.read_conll_file(dev_path / dev_file, "gold")
        for each_sent in sentence_list:
            each_sent.full_graph = graph_structure.create_graph_with_all_arcs_correctly(each_sent)
            each_sent.pred_graph = cle.chu_liu_edmonds_algorithm(each_sent.full_graph)
            #drawing.draw_graph(each_sent.pred_graph)
            print("full: ", each_sent.full_graph.arcs_dict)
            print("pred: ", each_sent.pred_graph.arcs_dict)
            for token in range(1, len(each_sent.token_list)):
                each_sent.token_list[token].pred_head = list(each_sent.pred_graph.nodes_dict[token].heads.keys())[0]
        results = eval.evaluate_list(sentence_list)
        # return token_count, correct_head_count, UAS,
        self.assertEqual(8, results[0]) # number of tokens without 0 / non-empty lines
        self.assertEqual(8, results[1]) # correct_head_count
        self.assertEqual(100.0, results[2]) # UAS
        #eval.check_sentence(each_sent)
        #raw.write_pred_conll_file(train_path / 'wsj_train.first-1k.conll06.pred', sentence_list_1k)


    def test_cle_cycles(self):
        full_graph_optimal = graph_structure.create_graph_from_dict({0: {1: 9, 2: 13, 3: 0, 4: 22, 5: 20, 6: 0, 7: 14, 8: 8},
               1: {2: 4, 3: 2, 4: 17, 5: 20, 6: 1, 7: 14, 8: 0}, 2: {1: 27, 3: 0, 4: 5, 5: 18, 6: 5, 7: 6, 8: 14},
               3: {1: 1, 2: 19, 4: 15, 5: 17, 6: 20, 7: 4, 8: 15}, 4: {1: 15, 2: 21, 3: 22, 5: 27, 6: 17, 7: 19, 8: 27},
               5: {1: 2, 2: 8, 3: 6, 4: 20, 6: 2, 7: 21, 8: 2}, 6: {1: 10, 2: 18, 3: 1, 4: 2, 5: 15, 7: 11, 8: 11},
               7: {1: 2, 2: 8, 3: 2, 4: 6, 5: 1, 6: 24, 8: 12}, 8: {1: 10, 2: 10, 3: 3, 4: 1, 5: 20, 6: 3, 7: 15}})
        full_graph_optimal = graph_structure.create_graph_from_dict(
            {0: {1: 9, 2: 13, 3: 0, 4: 22, 5: 20, 6: 0, 7: 14, 8: 8},
             1: {2: 4, 3: 2, 4: 17, 5: 20, 6: 1, 7: 14, 8: 0}, 2: {1: 27, 3: 0, 4: 5, 5: 18, 6: 5, 7: 6, 8: 14},
             3: {1: 1, 2: 19, 4: 15, 5: 17, 6: 20, 7: 4, 8: 15}, 4: {1: 15, 2: 21, 3: 22, 5: 27, 6: 17, 7: 19, 8: 27},
             5: {1: 2, 2: 8, 3: 6, 4: 20, 6: 2, 7: 21, 8: 2}, 6: {1: 10, 2: 18, 3: 1, 4: 2, 5: 15, 7: 11, 8: 11},
             7: {1: 2, 2: 8, 3: 2, 4: 6, 5: 1, 6: 24, 8: 12}, 8: {1: 10, 2: 10, 3: 3, 4: 1, 5: 20, 6: 3, 7: 15}})
        gold_graph= graph_structure.create_graph_from_dict({0: {4: 22}, 1: {}, 2: {1: 27}, 3: {}, 4: {2: 21, 3: 22,
                                                            5: 27, 8: 27}, 5: {7: 21}, 6: {}, 7: {6: 24}, 8: {}})

    def test_cle_for_example_sentence(self):
        #graph = graph_structure.create_graph_from_dict({0: {1: 7, 2: 25, 3: 5, 4: 6, 5: 2, 6: 14, 7: 10, 8: 18, 9: 14, 10: 10, 11: 15, 12: 3, 13: 6, 14: 20, 15: 16, 16: 16, 17: 0, 18: 6, 19: 18, 20: 8, 21: 19, 22: 5, 23: 10, 24: 4, 25: 1}, 1: {2: 25, 3: 13, 4: 18, 5: 9, 6: 15, 7: 15, 8: 7, 9: 20, 10: 17, 11: 18, 12: 17, 13: 1, 14: 1, 15: 14, 16: 5, 17: 15, 18: 0, 19: 14, 20: 3, 21: 19, 22: 12, 23: 9, 24: 5, 25: 7}, 2: {1: 3, 3: 19, 4: 7, 5: 17, 6: 4, 7: 0, 8: 14, 9: 15, 10: 1, 11: 6, 12: 18, 13: 5, 14: 9, 15: 4, 16: 5, 17: 11, 18: 3, 19: 6, 20: 10, 21: 11, 22: 19, 23: 6, 24: 18, 25: 0}, 3: {1: 12, 2: 30, 4: 15, 5: 15, 6: 16, 7: 6, 8: 8, 9: 14, 10: 11, 11: 5, 12: 8, 13: 20, 14: 11, 15: 14, 16: 18, 17: 16, 18: 15, 19: 18, 20: 11, 21: 19, 22: 12, 23: 15, 24: 15, 25: 12}, 4: {1: 0, 2: 5, 3: 22, 5: 0, 6: 11, 7: 17, 8: 7, 9: 11, 10: 20, 11: 3, 12: 20, 13: 5, 14: 12, 15: 13, 16: 16, 17: 9, 18: 7, 19: 5, 20: 6, 21: 11, 22: 6, 23: 13, 24: 9, 25: 20}, 5: {1: 8, 2: 19, 3: 17, 4: 10, 6: 2, 7: 19, 8: 2, 9: 28, 10: 11, 11: 19, 12: 12, 13: 14, 14: 20, 15: 8, 16: 8, 17: 17, 18: 15, 19: 9, 20: 18, 21: 15, 22: 5, 23: 19, 24: 18, 25: 0}, 6: {1: 6, 2: 10, 3: 6, 4: 1, 5: 8, 7: 0, 8: 13, 9: 27, 10: 7, 11: 20, 12: 12, 13: 1, 14: 9, 15: 18, 16: 2, 17: 9, 18: 6, 19: 5, 20: 12, 21: 15, 22: 12, 23: 19, 24: 9, 25: 17}, 7: {1: 6, 2: 14, 3: 4, 4: 5, 5: 16, 6: 28, 8: 20, 9: 0, 10: 20, 11: 14, 12: 20, 13: 13, 14: 20, 15: 14, 16: 16, 17: 12, 18: 0, 19: 15, 20: 10, 21: 1, 22: 12, 23: 3, 24: 11, 25: 14}, 8: {1: 20, 2: 10, 3: 18, 4: 17, 5: 11, 6: 5, 7: 22, 9: 2, 10: 3, 11: 0, 12: 8, 13: 3, 14: 1, 15: 4, 16: 20, 17: 19, 18: 8, 19: 8, 20: 10, 21: 16, 22: 4, 23: 9, 24: 7, 25: 16}, 9: {1: 3, 2: 12, 3: 8, 4: 21, 5: 13, 6: 3, 7: 18, 8: 20, 10: 3, 11: 13, 12: 16, 13: 17, 14: 8, 15: 18, 16: 5, 17: 13, 18: 13, 19: 17, 20: 9, 21: 5, 22: 11, 23: 17, 24: 9, 25: 6}, 10: {1: 10, 2: 7, 3: 20, 4: 10, 5: 3, 6: 13, 7: 6, 8: 10, 9: 18, 11: 28, 12: 19, 13: 11, 14: 8, 15: 6, 16: 11, 17: 14, 18: 1, 19: 15, 20: 12, 21: 19, 22: 15, 23: 5, 24: 5, 25: 10}, 11: {1: 17, 2: 1, 3: 27, 4: 11, 5: 20, 6: 19, 7: 0, 8: 7, 9: 4, 10: 14, 12: 18, 13: 6, 14: 17, 15: 6, 16: 15, 17: 19, 18: 9, 19: 18, 20: 14, 21: 2, 22: 18, 23: 13, 24: 5, 25: 15}, 12: {1: 19, 2: 14, 3: 4, 4: 20, 5: 9, 6: 19, 7: 15, 8: 6, 9: 17, 10: 8, 11: 28, 13: 6, 14: 14, 15: 6, 16: 9, 17: 4, 18: 18, 19: 19, 20: 16, 21: 19, 22: 2, 23: 15, 24: 9, 25: 17}, 13: {1: 8, 2: 11, 3: 15, 4: 5, 5: 12, 6: 20, 7: 20, 8: 12, 9: 18, 10: 11, 11: 6, 12: 14, 14: 28, 15: 12, 16: 19, 17: 8, 18: 18, 19: 6, 20: 14, 21: 4, 22: 16, 23: 5, 24: 4, 25: 12}, 14: {1: 11, 2: 4, 3: 3, 4: 3, 5: 11, 6: 1, 7: 8, 8: 18, 9: 9, 10: 1, 11: 8, 12: 22, 13: 20, 15: 12, 16: 14, 17: 19, 18: 6, 19: 19, 20: 1, 21: 15, 22: 7, 23: 5, 24: 13, 25: 5}, 15: {1: 18, 2: 20, 3: 18, 4: 4, 5: 16, 6: 13, 7: 3, 8: 5, 9: 3, 10: 11, 11: 7, 12: 4, 13: 10, 14: 22, 16: 1, 17: 7, 18: 9, 19: 13, 20: 19, 21: 5, 22: 5, 23: 6, 24: 6, 25: 2}, 16: {1: 14, 2: 8, 3: 8, 4: 4, 5: 19, 6: 13, 7: 2, 8: 1, 9: 1, 10: 6, 11: 6, 12: 6, 13: 11, 14: 3, 15: 30, 17: 12, 18: 9, 19: 12, 20: 11, 21: 20, 22: 7, 23: 10, 24: 2, 25: 13}, 17: {1: 10, 2: 6, 3: 15, 4: 6, 5: 1, 6: 12, 7: 0, 8: 18, 9: 16, 10: 11, 11: 8, 12: 12, 13: 3, 14: 4, 15: 3, 16: 27, 18: 19, 19: 2, 20: 13, 21: 11, 22: 0, 23: 8, 24: 3, 25: 2}, 18: {1: 12, 2: 11, 3: 12, 4: 5, 5: 1, 6: 11, 7: 8, 8: 0, 9: 16, 10: 6, 11: 6, 12: 7, 13: 17, 14: 10, 15: 10, 16: 1, 17: 13, 19: 27, 20: 13, 21: 17, 22: 9, 23: 12, 24: 5, 25: 13}, 19: {1: 4, 2: 11, 3: 19, 4: 18, 5: 18, 6: 10, 7: 5, 8: 10, 9: 17, 10: 8, 11: 18, 12: 13, 13: 15, 14: 12, 15: 19, 16: 20, 17: 30, 18: 1, 20: 0, 21: 7, 22: 7, 23: 16, 24: 5, 25: 19}, 20: {1: 19, 2: 6, 3: 2, 4: 0, 5: 4, 6: 19, 7: 14, 8: 4, 9: 18, 10: 2, 11: 7, 12: 6, 13: 16, 14: 19, 15: 18, 16: 15, 17: 0, 18: 9, 19: 22, 21: 0, 22: 14, 23: 4, 24: 15, 25: 10}, 21: {1: 15, 2: 8, 3: 1, 4: 5, 5: 8, 6: 16, 7: 10, 8: 7, 9: 1, 10: 1, 11: 19, 12: 5, 13: 18, 14: 2, 15: 19, 16: 11, 17: 16, 18: 5, 19: 16, 20: 21, 22: 8, 23: 13, 24: 16, 25: 18}, 22: {1: 15, 2: 10, 3: 9, 4: 5, 5: 17, 6: 7, 7: 20, 8: 20, 9: 19, 10: 8, 11: 19, 12: 13, 13: 16, 14: 20, 15: 10, 16: 11, 17: 15, 18: 9, 19: 22, 20: 0, 21: 13, 23: 16, 24: 13, 25: 11}, 23: {1: 13, 2: 4, 3: 12, 4: 7, 5: 16, 6: 9, 7: 18, 8: 0, 9: 13, 10: 15, 11: 13, 12: 1, 13: 1, 14: 12, 15: 15, 16: 14, 17: 10, 18: 18, 19: 19, 20: 4, 21: 10, 22: 2, 24: 28, 25: 11}, 24: {1: 11, 2: 19, 3: 1, 4: 18, 5: 11, 6: 19, 7: 12, 8: 9, 9: 2, 10: 17, 11: 16, 12: 8, 13: 15, 14: 20, 15: 2, 16: 20, 17: 12, 18: 1, 19: 0, 20: 11, 21: 9, 22: 29, 23: 20, 25: 1}, 25: {1: 18, 2: 28, 3: 11, 4: 1, 5: 15, 6: 15, 7: 5, 8: 18, 9: 13, 10: 4, 11: 11, 12: 11, 13: 5, 14: 9, 15: 8, 16: 7, 17: 9, 18: 5, 19: 19, 20: 8, 21: 9, 22: 16, 23: 14, 24: 14}})
        #graph = graph_structure.create_graph_from_dict({0: {0: 21, 1: 10, 2: 14, 3: 15, 4: 3, 5: 10, 6: 7, 7: 22, 8: 3, 9: 17, 10: 3, 11: 19, 12: 18}, 1: {1: 3, 2: 9, 3: 9, 4: 27, 5: 15, 6: 13, 7: 14, 8: 3, 9: 4, 10: 2, 11: 11, 12: 13}, 2: {1: 9, 2: 2, 3: 21, 4: 17, 5: 4, 6: 10, 7: 20, 8: 18, 9: 3, 10: 15, 11: 13, 12: 2}, 3: {1: 11, 2: 7, 3: 14, 4: 22, 5: 12, 6: 14, 7: 20, 8: 1, 9: 0, 10: 2, 11: 18, 12: 19}, 4: {1: 1, 2: 18, 3: 10, 4: 5, 5: 7, 6: 6, 7: 23, 8: 2, 9: 20, 10: 13, 11: 19, 12: 19}, 5: {1: 3, 2: 6, 3: 16, 4: 5, 5: 12, 6: 21, 7: 11, 8: 17, 9: 4, 10: 5, 11: 2, 12: 4}, 6: {1: 9, 2: 7, 3: 19, 4: 15, 5: 0, 6: 17, 7: 25, 8: 13, 9: 16, 10: 17, 11: 16, 12: 11}, 7: {1: 10, 2: 15, 3: 3, 4: 15, 5: 16, 6: 12, 7: 10, 8: 16, 9: 0, 10: 5, 11: 7, 12: 5}, 8: {1: 2, 2: 9, 3: 10, 4: 4, 5: 20, 6: 4, 7: 5, 8: 10, 9: 28, 10: 16, 11: 8, 12: 10}, 9: {1: 8, 2: 2, 3: 1, 4: 11, 5: 4, 6: 14, 7: 24, 8: 6, 9: 12, 10: 10, 11: 3, 12: 0}, 10: {1: 7, 2: 18, 3: 3, 4: 12, 5: 12, 6: 16, 7: 26, 8: 14, 9: 11, 10: 10, 11: 9, 12: 6}, 11: {1: 0, 2: 1, 3: 13, 4: 17, 5: 11, 6: 4, 7: 20, 8: 12, 9: 1, 10: 15, 11: 4, 12: 27}, 12: {1: 13, 2: 8, 3: 20, 4: 6, 5: 9, 6: 17, 7: 16, 8: 5, 9: 6, 10: 21, 11: 11, 12: 1}})
        # 3rd sentence with 12 tokens
        graph = graph_structure.create_graph_from_dict({0: {1: 1, 2: 12, 3: 1, 4: 12, 5: 6, 6: 2, 7: 27, 8: 0, 9: 12, 10: 16, 11: 14, 12: 5}, 1: {2: 0, 3: 14, 4: 29, 5: 8, 6: 2, 7: 12, 8: 1, 9: 1, 10: 14, 11: 9, 12: 0}, 2: {1: 5, 3: 25, 4: 12, 5: 5, 6: 18, 7: 12, 8: 20, 9: 15, 10: 5, 11: 14, 12: 3}, 3: {1: 12, 2: 12, 4: 26, 5: 1, 6: 18, 7: 6, 8: 1, 9: 13, 10: 5, 11: 20, 12: 4}, 4: {1: 18, 2: 17, 3: 4, 5: 5, 6: 19, 7: 29, 8: 11, 9: 5, 10: 13, 11: 16, 12: 11}, 5: {1: 1, 2: 3, 3: 19, 4: 0, 6: 21, 7: 3, 8: 15, 9: 3, 10: 15, 11: 14, 12: 17}, 6: {1: 10, 2: 16, 3: 2, 4: 12, 5: 14, 7: 21, 8: 8, 9: 2, 10: 6, 11: 16, 12: 5}, 7: {1: 18, 2: 8, 3: 10, 4: 4, 5: 6, 6: 1, 8: 10, 9: 11, 10: 1, 11: 5, 12: 5}, 8: {1: 16, 2: 17, 3: 12, 4: 10, 5: 6, 6: 0, 7: 10, 9: 28, 10: 2, 11: 3, 12: 8}, 9: {1: 7, 2: 19, 3: 12, 4: 10, 5: 9, 6: 16, 7: 21, 8: 13, 10: 17, 11: 15, 12: 4}, 10: {1: 10, 2: 12, 3: 1, 4: 4, 5: 12, 6: 10, 7: 27, 8: 9, 9: 1, 11: 0, 12: 18}, 11: {1: 8, 2: 20, 3: 2, 4: 2, 5: 20, 6: 10, 7: 12, 8: 2, 9: 11, 10: 17, 12: 23}, 12: {1: 4, 2: 11, 3: 9, 4: 1, 5: 8, 6: 7, 7: 11, 8: 11, 9: 0, 10: 28, 11: 4}})
        resolved_graph = cle.chu_liu_edmonds_algorithm(graph)


    def test_check_time_efficiency(self):
        train_path = Path().resolve().parent / 'data' / 'english' / 'train'
        dev_path = Path().resolve().parent / 'data' / 'english' / 'dev'
        test_path = Path().resolve().parent / 'data' / 'english' / 'test'
        train_file = 'wsj_train.conll06'
        dev_file = 'wsj_dev.conll06.gold'
        test_file = 'wsj_test.conll06.blind'

        sentence_list_train = raw.read_conll_file(train_path / train_file, "gold")
        sentence_list_dev = raw.read_conll_file(dev_path / dev_file, "gold")
        sentence_list_test = raw.read_conll_file(test_path / test_file, "blind")
        i = 0
        for each_sent in sentence_list_train:
            sen_len = len(each_sent.token_list)
            if sen_len == 72:
                print("Sentence ", i, ": ", sen_len, " tokens")
            # each_sent.full_graph = graph_structure.create_graph_with_all_arcs_randomly(each_sent)
            # each_sent.final_graph = cle.chu_liu_edmonds_algorithm(each_sent.full_graph)
            # for token in range(1, len(each_sent.token_list)):
            #     each_sent.token_list[token].pred_head = list(each_sent.final_graph.nodes_dict[token].heads.keys())[0]
            i += 1
        #results = eval.evaluate_list(sentence_list)


    def test_time_efficiency(self):
        train_path = Path().resolve().parent / 'data' / 'english' / 'train'
        train_file = 'wsj_train.conll06'
        # code snippet to be executed only once
        test_setup = "from decoding import chu_liu_edmonds as cle;from graphs import exercise_graph, graph_structure;" \
                  "from pathlib import Path;from reader_and_writer import reader_and_writer as raw;sentence_list_train=" \
                  "raw.read_conll_file(Path().resolve().parent/'data'/'english'/'train'/'wsj_train.conll06','gold');" \
                  "sent1 = sentence_list_train[1198];sent2 = sentence_list_train[13606];graph1 = " \
                  "graph_structure.create_graph_with_all_arcs_randomly(sent1);graph2 = " \
                  "graph_structure.create_graph_with_all_arcs_randomly(sent2)"


        time1 = timeit.timeit(stmt='cle.chu_liu_edmonds_algorithm(graph1)', setup=test_setup, number=1000)
        print("CLE for a sentence with 72 tokens takes ", time1, " seconds.")
        time2 = timeit.timeit(stmt='cle.chu_liu_edmonds_algorithm(graph2)', setup=test_setup, number=1000)
        print("CLE for a sentence with 144 tokens takes ", time2, " seconds.")
        ratio = time2 / time1
        print("That's ", ratio, " times as long.")

