import unittest

from machine_learning import feature_extractor as fe
from graphs import graph_structure
from reader_and_writer import reader_and_writer as raw
from pathlib import Path

class test_feature_extractor(unittest.TestCase):

    train_path = Path().resolve().parent / 'data' / 'english' / 'train'
    train_file = 'wsj_train.first-1k.conll06'
    sentence_list_1k = raw.read_conll_file(train_path / train_file, "gold")


    def test_extract_features(self):

        feat_nr = 15 # number of implemented feature templates

        sent = self.sentence_list_1k[1]
        sent_len_wo_root = len(sent.token_list) - 1 # number of tokens without the root token

        # ROOT node has (sent_len_wo_root) nr of arcs, each other node has one arc less because ROOT can't be dependent
        nr_of_arcs = sent_len_wo_root + (sent_len_wo_root * (sent_len_wo_root-1))
        expected_feat_nr = nr_of_arcs * feat_nr

        graph = graph_structure.create_graph_with_all_arcs_randomly(sent)

        feat_ext = fe.Feature_extractor()
        feat_ext.extract_features(sent, False)

        actual_feat_nr = 0
        for arc, feat_list in graph.arcs_feat.items():
            actual_feat_nr += len(feat_list)

        unique_feat = feat_ext.string_mapper.feat_counter

        # check correct number of features
        self.assertGreaterEqual(expected_feat_nr, actual_feat_nr)

        # ensures that number of unique features isn't higher than the number of total features
        self.assertLessEqual(unique_feat, actual_feat_nr)

        shortest_str = "hpos=x+l+0"
        len_ss = len(shortest_str)

        # ensures that each feature value is of reasonable length
        for i in range(0, unique_feat):
            value_str = feat_ext.string_mapper.inverse_lookup(i)
            print(value_str)
            self.assertGreaterEqual(len(value_str), len_ss)



    def test_extract_all_features(self):

        for sent in self.sentence_list_1k:
            graph_structure.create_graph_with_all_arcs_randomly(sent)

        feat_ext = fe.Feature_extractor()
        feat_ext.extract_all_features(self.sentence_list_1k, False)

        shortest_str = "hpos=x+l+0"
        len_ss = len(shortest_str)

        feat_nr = 15  # number of implemented feature templates

        feat_sum = 0

        sent_nr = 0
        for sent in self.sentence_list_1k:
            sent_len_wo_root = len(sent.token_list) - 1  # number of tokens without the root token

            # ROOT node has (sent_len_wo_root) nr of arcs, each other node has one arc less because ROOT can't be dependent
            nr_of_arcs = sent_len_wo_root + (sent_len_wo_root * (sent_len_wo_root - 1))
            expected_feat_nr = nr_of_arcs * feat_nr

            actual_feat_nr = 0
            for arc, feat_list in sent.full_graph.arcs_feat.items():
                feat_sum += len(feat_list)
                actual_feat_nr += len(feat_list)

            # check correct number of features
            self.assertEqual(expected_feat_nr, actual_feat_nr, ("Sentence nr. " + str(sent_nr) + " should have " +
                             str(expected_feat_nr) + " in total but actual count is " +  str(actual_feat_nr) + "!"))

            sent_nr += 1

        unique_feat = feat_ext.string_mapper.feat_counter

        # ensures that each feature value is of reasonable length
        for i in range(1, unique_feat):
            value_str = feat_ext.string_mapper.inverse_lookup(i)
            self.assertGreaterEqual(len(value_str), len_ss)

        # ensures that number of unique features isn't higher than the number of total features
        self.assertLessEqual(unique_feat, feat_sum)
