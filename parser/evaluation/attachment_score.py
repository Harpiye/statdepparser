from reader_and_writer import reader_and_writer as raw
from pathlib import Path
import unittest

class evaluation_score:
    # unlabeled attachment score: nr of tokens with correct head / nr of tokens
    UAS = 0
    # labeled attachment score: nr of tokens with correct head and correct label / nr of tokens
    LAS = 0


def calc_UAS(correct_heads, tokens):
    UAS = round((float(correct_heads) / float(tokens) * 100), 2)
    return UAS


def calc_LAS(correct_heads_and_labels, tokens):
    LAS = round((float(correct_heads_and_labels) / float(tokens) * 100), 2)
    return LAS


def evaluate(sentence_list_pred, sentence_list_gold):
    correct_head_count = 0
    correct_head_and_label_count = 0
    token_count = 0

    for i in range(0, len(sentence_list_pred)):
        curr_pred_list = sentence_list_pred[i].token_list
        resp_gold_list = sentence_list_gold[i].token_list
        # skip first token of each sentence because it's just a "root" entry
        for j in range (1, len(curr_pred_list)):
            token_count += 1
            if curr_pred_list[j].pred_head == resp_gold_list[j].gold_head:
                correct_head_count += 1
                if curr_pred_list[j].pred_rel == resp_gold_list[j].gold_rel:
                    correct_head_and_label_count +=1
            #else:
                #print("Head of ", curr_pred_list[j].id, ". token should be ", resp_gold_list[j].gold_head, " but is ",
                #      curr_pred_list[j].pred_head, "!")
    UAS = calc_UAS(correct_head_count, token_count)
    LAS = calc_LAS(correct_head_and_label_count, token_count)
    #print("\nScores are:")
    #print("UAS: " +  str(correct_head_count) + "/" + str(token_count) + " = " + str(UAS) + "%")
    #print("LAS: " + str(correct_head_and_label_count) + "/" + str(token_count) + " = " + str(LAS) + "%")
    #print("\n")
    return token_count, correct_head_count, UAS, correct_head_and_label_count, LAS


def evaluate_list(sentence_list_both):
    correct_head_count = 0
    correct_head_and_label_count = 0
    token_count = 0

    for i in range(0, len(sentence_list_both)):
        #curr_pred_list = sentence_list_pred[i].token_list
        curr_token_list = sentence_list_both[i].token_list
        # skip first token of each sentence because it's just a "root" entry
        for j in range (1, len(curr_token_list)):
            token_count += 1
            if curr_token_list[j].pred_head == curr_token_list[j].gold_head:
                correct_head_count += 1
                if curr_token_list[j].pred_rel == curr_token_list[j].gold_rel:
                    correct_head_and_label_count +=1
    UAS = calc_UAS(correct_head_count, token_count)
    LAS = calc_LAS(correct_head_and_label_count, token_count)
    #print("Scores are:\n")
    #print("UAS: " +  str(correct_head_count) + "/" + str(token_count) + " = " + str(UAS) + "%")
    #print("LAS: " + str(correct_head_and_label_count) + "/" + str(token_count) + " = " + str(LAS) + "%")
    #print("\n")
    return token_count, correct_head_count, UAS, correct_head_and_label_count, LAS


def check_sentence(sentence):
    correct_head_count = 0
    correct_head_and_label_count = 0
    token_count = 0

    #curr_pred_list = sentence_list_pred[i].token_list
    curr_token_list = sentence.token_list
    # skip first token of each sentence because it's just a "root" entry
    for j in range (1, len(curr_token_list)):
        token_count += 1
        if curr_token_list[j].pred_head == curr_token_list[j].gold_head:
            correct_head_count += 1
        else:
            print("Correct head for " + str(j) + " should be " + str(curr_token_list[j].gold_head) + " but is " +
                  str(curr_token_list[j].pred_head) + "!")

    UAS = calc_UAS(correct_head_count, token_count)
    print("Scores are:\n")
    print("UAS: " +  str(correct_head_count) + "/" + str(token_count) + " = " + str(UAS) + "%")
    print("\n")





