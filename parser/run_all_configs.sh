#!/bin/bash

for i in {1..2};do
  screen -S "etest$i" python eng_pipe.py -c "config/config_$i.json" -s 1h;
  screen -S "dtest$i" python ger_pipe.py -c "config/config_$i.json" -s 1h;
done