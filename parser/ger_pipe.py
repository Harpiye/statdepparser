from machine_learning.perceptron import trainer, trainer_pipe
import argparse
import json


if __name__ == "__main__":
    # Parse command line arguments.
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", dest="config",
                        help="Absolute path to configuration file.")
    parser.add_argument("-s" "--size", dest="size",
                        help="Run the perceptron for either 1h, 1k or the full data sets.")
    args = parser.parse_args()

    # Ensure a config was passed to the script.
    if not args.config:
        print("No configuration file provided.")
        exit()
    else:
        if not args.size:
            print("No size of data sets specified.")
            exit()
        else:
            with open(args.config, "r") as inp:
                config = json.load(inp)

                trainer_pipe.german_pipeline(args.size, **config["trainer"])
