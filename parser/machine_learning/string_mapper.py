
class String_Mapper:

    # dict to store a uniquely identifiable ID with its (actually also unique) string values and vice versa 
    feat_dict = {} # key: feature string, value: uniquely identifiable ID
    feat_list = [] # list index: ID starting with 0, value: feature string
    feat_counter = 0
    freeze = False

    def lookup(self, feat_string):
        """
        Look up whether a feature string was already seen
        - if it is, return the index
        - if not, generate a new index, store the string, and look it up

        :param feat_string: feature string to lookup
        :return: the value to the key-string
        """
        # if the string isn't already known to the mapper...
        if (self.feat_dict.get(feat_string) is None):
            # ... we're still in the training phase and learning new features
            if (self.freeze==False):
                self.feat_dict[feat_string] = self.feat_counter
                self.feat_list.append(feat_string)
                index = self.feat_counter
                # index for the next inserted string will be increased by 1
                self.feat_counter += 1
            else: # return -1 to indicate that this feature can't be added anymore
                index = -1
        else:
            index = self.feat_dict.get(feat_string)
        return index


    def inverse_lookup(self, feat_index):
        """
        Look up the feature string corresponding to a feature index.

        :param feat_index: ID of a feature
        :return: corresponding feature string to a given index
        """
        feat_string = self.feat_list[feat_index]
        return feat_string