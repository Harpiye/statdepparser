
from machine_learning import string_mapper as sm
import sys

class Feature_extractor:

    def __init__(self, string_mapper=None):
        self.string_mapper = sm.String_Mapper() if string_mapper is None else string_mapper

    def extract_all_features(self, sentence_list, feature_selector, german_flag):
        """
        Extract all available features for all supplied sentences.

        :return:
        """
        for each_sent in sentence_list:
            self.extract_features(each_sent, feature_selector, german_flag)


    def extract_features(self, sentence, feature_selector, german_flag):
        """
        Extract all features for a sentence.

        :return:
        """
        #features = {}
        #feature_values = {}
        for arc, feat_vector in sentence.full_graph.arcs_feat.items():
            head = arc[0] # pos. of head in sentence / id
            dependent = arc[1] # pos. of dependent in sentence / id
            #print("arc: ", head, " -> ", dependent)
            apx = ""
            rev = False
            if head < dependent: # e.g. 1 -> 3
                apx += "r"
                d = dependent - head - 1
            else: # e.g. 3 -> 1
                apx += "l"
                rev = True
                d = head - dependent - 1
            if d > 4:
                d = "4+"
            else:
                d = str(d)
            apx += "+" + d

            hform = ""
            hpos = ""
            dform = ""
            dpos = ""

            # Unigram features from McDonald et al. (2005):
            # create feature and append to feature vector
            if feature_selector["unigram_basis"]:
                hform = "hform=" + sentence.token_list[arc[0]].form
                feat_vector.append(self.basic_feature_template(hform, apx))
                hpos = "hpos=" + str(sentence.token_list[arc[0]].pos)
                feat_vector.append(self.basic_feature_template(hpos, apx))
                feat_vector.append(self.combi_feature_template([hform, hpos], apx))
                dform = "dform=" + sentence.token_list[arc[1]].form
                feat_vector.append(self.basic_feature_template(dform, apx))
                dpos = "dpos=" + str(sentence.token_list[arc[1]].pos)
                feat_vector.append(self.basic_feature_template(dpos, apx))
                feat_vector.append(self.combi_feature_template([dform, dpos], apx))

            hlemma = ""
            dlemma = ""
            if feature_selector["single_lemma"]:
                if (sentence.token_list[head].lemma!=sentence.token_list[head].form):
                    hlemma = "hlemma" + sentence.token_list[head].lemma
                    feat_vector.append(self.basic_feature_template(hlemma, apx))
                    # additional Unigram features including lemma
                    feat_vector.append(self.combi_feature_template([hlemma, hpos], apx))
                    # additional Bigram features including lemma
                    feat_vector.append(self.combi_feature_template([hlemma, hpos, dpos], apx))

                if sentence.token_list[dependent].lemma != sentence.token_list[dependent].form:
                    dlemma = "dlemma" + sentence.token_list[dependent].lemma
                    feat_vector.append(self.basic_feature_template(dlemma, apx))
                    # additional Unigram features including lemma
                    feat_vector.append(self.combi_feature_template([dlemma, dpos], apx))
                    # additional Bigram features including lemma
                    feat_vector.append(self.combi_feature_template([hpos, dlemma, dpos], apx))

            # Bigram features
            if (feature_selector["bigram_basis"]):
                feat_vector.append(self.combi_feature_template([hform, hpos, dform, dpos], apx))
                feat_vector.append(self.combi_feature_template([hpos, dform, dpos], apx))
                feat_vector.append(self.combi_feature_template([hform, dform, dpos], apx))
                feat_vector.append(self.combi_feature_template([hform, hpos, dform], apx))
                feat_vector.append(self.combi_feature_template([hform, hpos, dpos], apx))
                feat_vector.append(self.combi_feature_template([hform, dform], apx))
                feat_vector.append(self.combi_feature_template([hpos, dpos], apx))

            if feature_selector["bigram_lemma"] and len(dlemma)>0 and len(hlemma)>0:
                # more additional Bigram features including lemmas
                feat_vector.append(self.combi_feature_template([hlemma, dlemma], apx))
                feat_vector.append(self.combi_feature_template([hlemma, hpos, dlemma, dpos], apx))
                feat_vector.append(self.combi_feature_template([hlemma, dlemma, dpos], apx))
                feat_vector.append(self.combi_feature_template([hlemma, hpos, dlemma], apx))

            # Other
            bpos = ""
            if feature_selector["other_basis"]:
                if (rev==False):
                    for i in range(arc[0]+1, arc[1]):
                        bpos += sentence.token_list[i].pos + "+"
                else:
                    for i in range(arc[1]+1, arc[0]):
                        bpos += sentence.token_list[i].pos + "+"
                if len(bpos) > 0:
                    bpos = bpos[:-1]
                    if feature_selector["bpos"]:
                        feat_vector.append(self.basic_feature_template("bpos=" + bpos, apx))
                    feat_vector.append(self.combi_feature_template([hpos, bpos, dpos], apx))

                ran = len(sentence.token_list)

                hpos_m1 = ""
                if (head-1) >= 0:
                    hpos_m1 = "hposm1=" + sentence.token_list[head-1].pos
                    if feature_selector["single_neighbor"]:
                        feat_vector.append(self.basic_feature_template(hpos_m1, apx))

                dpos_m1 = ""
                if (dependent-1) >= 0:
                    dpos_m1 = "dposm1=" + sentence.token_list[dependent-1].pos
                    if feature_selector["single_neighbor"]:
                        feat_vector.append(self.basic_feature_template(dpos_m1, apx))

                hpos_p1 = ""
                if (head+1) < ran:
                    hpos_p1 = "hposp1=" + sentence.token_list[head+1].pos
                    if feature_selector["single_neighbor"]:
                        feat_vector.append(self.basic_feature_template(hpos_p1, apx))

                dpos_p1 = ""
                if (dependent+1) < ran:
                    dpos_p1 = "dposp1=" + sentence.token_list[dependent+1].pos
                    if feature_selector["single_neighbor"]:
                        feat_vector.append(self.basic_feature_template(dpos_p1, apx))

                if (len(hpos_p1) > 0) and (len(dpos_m1)):
                    feat_vector.append(self.combi_feature_template([hpos, dpos, hpos_p1, dpos_m1], apx))
                if (len(hpos_m1) > 0) and (len(dpos_m1)):
                    feat_vector.append(self.combi_feature_template([hpos, dpos, hpos_m1, dpos_m1], apx))
                if (len(hpos_p1) > 0) and (len(dpos_p1) > 0):
                    feat_vector.append(self.combi_feature_template([hpos, dpos, hpos_p1, dpos_p1], apx))
                if (len(hpos_m1) > 0) and (len(dpos_p1) > 0):
                    feat_vector.append(self.combi_feature_template([hpos, dpos, hpos_m1, dpos_p1], apx))

            if feature_selector["root"]:
                hdroot = "hdroot=" + str((sentence.token_list[head].id)-1)
                feat_vector.append(self.basic_feature_template(hdroot, apx))
                ddroot = "ddroot=" + str((sentence.token_list[dependent].id)-1)
                feat_vector.append(self.basic_feature_template(ddroot, apx))
                feat_vector.append(self.combi_feature_template([hdroot, ddroot], apx))

            if (german_flag):
                hmorph = "hmorph=" + sentence.token_list[head].morph
                hmorph_array = []
                dmorph = "dmorph=" + sentence.token_list[dependent].morph
                dmorph_array = []


                if (len(hmorph) > 7):
                    if feature_selector["unigram_morph"]:
                        feat_vector.append(self.basic_feature_template(hmorph, apx))
                        feat_vector.append(self.combi_feature_template([hpos, hmorph], apx))
                        feat_vector.append(self.combi_feature_template([hlemma, hpos, hmorph], apx))
                        feat_vector.append(self.combi_feature_template([hform, hpos, hmorph], apx))

                    if feature_selector["single_morph"]:
                        hmorph_array = sentence.token_list[head].morph.split("|")
                        for each in hmorph_array:
                            feat_vector.append(self.basic_feature_template("h" + each, apx))


                if (len(dmorph) > 7):
                    if feature_selector["unigram_morph"]:
                        feat_vector.append(self.basic_feature_template(dmorph, apx))
                        feat_vector.append(self.combi_feature_template([dpos, dmorph], apx))
                        feat_vector.append(self.combi_feature_template([dlemma, dpos, dmorph], apx))
                        feat_vector.append(self.combi_feature_template([dform, dpos, dmorph], apx))

                    if feature_selector["single_morph"]:
                        dmorph_array = sentence.token_list[dependent].morph.split("|")
                        for each in dmorph_array:
                            feat_vector.append(self.basic_feature_template("d" + each, apx))


                if feature_selector["bigram_morph"]:
                    if (len(hmorph) > 7) and (len(dmorph) > 7):
                        feat_vector.append(self.combi_feature_template([hmorph, dmorph], apx))
                        feat_vector.append(self.combi_feature_template([hpos, hmorph, dpos, dmorph], apx))

                        for one in hmorph_array:
                            one_split = one.split("=")
                            for other in dmorph_array:
                                other_split = other.split("=")
                                if one_split[0]==other_split[0]:
                                    feat_vector.append(self.combi_feature_template(["h" + one, "d" + other], apx))

                    bmorph = ""
                    if len(bpos) > 0:
                        if (rev == False):
                            for i in range(arc[0] + 1, arc[1]):
                                bmorph += sentence.token_list[i].morph + "+"
                        else:
                            for i in range(arc[1] + 1, arc[0]):
                                bmorph += sentence.token_list[i].morph + "+"

                        bmorph = bmorph[:-1]
                        feat_vector.append(self.basic_feature_template("bmorph=" + bmorph, apx))
                        feat_vector.append(self.combi_feature_template([hmorph, bmorph, dmorph], apx))

            #print("feat_vector: ", feat_vector)
            if self.string_mapper.freeze:
                # removes all values in the vector which are -1 because these are features only observed in the
                # development or test set, which means they can't be trained anymore
                sentence.full_graph.arcs_feat[arc] = [x for x in feat_vector if x != -1]
            #print("arcs_feat: ", sentence.full_graph.arcs_feat)
            #print("arcs_dict: ", sentence.full_graph.arcs_dict)
            #print("arcs_feat[arc]: ", sentence.full_graph.arcs_feat[arc])


    def basic_feature_template(self, value, apx):
        feat_value = value + "+" + apx
        index = self.string_mapper.lookup(feat_value)
        #print("feat. ", index, ": ", feat_value)
        return index


    def combi_feature_template(self, value_list, apx):
        feat_value = ""
        for value in value_list:
            feat_value +=  value + "+"
        #feat_value = feat_value[:-1]
        feat_value += apx
        index = self.string_mapper.lookup(feat_value)
        #print("feat. ", index, ": ", feat_value)
        return index







