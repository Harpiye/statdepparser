from pathlib import Path
from graphs import graph_structure
from machine_learning import feature_extractor as fe
from reader_and_writer import reader_and_writer as raw
from reader_and_writer.saver_and_loader import *
from .structured_perceptron import Perceptron
from .averaged_perceptron import AveragedPerceptron

model_path = Path().cwd() / 'data' / 'output' / 'model'
pred_path = Path().cwd() / 'data' / 'output' / 'pred'


def english_avg_pipeline(file_prefix_store, file_prefix_load, shuffle_flag):

    sentence_list_train = []
    sentence_list_dev = []
    sentence_list_test = []

    feat_extractor = fe.Feature_extractor()

    perceptron = None

    if (file_prefix_load is None):
        # read necessary files
        sentence_list_train = raw.read_conll_file(raw.train_file_eng, "gold")
        sentence_list_dev = raw.read_conll_file(raw.dev_file_eng, "gold")
        sentence_list_test = raw.read_conll_file(raw.test_file_eng, "blind")

        # create all fully connected graphs
        graph_structure.create_all_full_graphs(sentence_list_train)
        graph_structure.create_all_full_graphs(sentence_list_dev)
        graph_structure.create_all_full_graphs(sentence_list_test)

        # create all gold graphs
        graph_structure.create_all_gold_graphs(sentence_list_train)
        graph_structure.create_all_gold_graphs(sentence_list_dev)

        # extract features for training set
        print("Extracting features for training set with ", len(sentence_list_train), " sentences...")
        feat_extractor.extract_all_features(sentence_list_train, german_flag=False)
        print(str(feat_extractor.string_mapper.feat_counter - 1) + " unique features extracted.")
        save_data(file_prefix_store + ".train", sentence_list_train)

        # set freeze flag to indicate that no new features can be added anymore
        feat_extractor.string_mapper.freeze = True
        # extract features for development and test set
        print("Extracting features for development and test set...")
        feat_extractor.extract_all_features(sentence_list_dev, False)
        save_data(file_prefix_store + ".dev", sentence_list_dev)
        feat_extractor.extract_all_features(sentence_list_test, False)
        save_data(file_prefix_store + ".test", sentence_list_test)

        perceptron = AveragedPerceptron(feat_extractor.string_mapper.feat_counter)

    else:
        sentence_list_train = load_data(file_prefix_load + ".train")
        sentence_list_dev = load_data(file_prefix_load + ".dev")
        sentence_list_test = load_data(file_prefix_load + ".test")

        perceptron = load_weights(file_prefix_load + ".weights")
        print(str(feat_extractor.string_mapper.feat_counter - 1) + " unique features loaded.")

    perceptron.avg_train(sentence_list_train, sentence_list_dev, sentence_list_test, file_prefix_store, 500, shuffle_flag)


def german_avg_pipeline(file_prefix_store, file_prefix_load, shuffle_flag):

    sentence_list_train = []
    sentence_list_dev = []
    sentence_list_test = []

    feat_extractor = fe.Feature_extractor()

    perceptron = None

    if (file_prefix_load is None):
        # read necessary files
        sentence_list_train = raw.read_conll_file(raw.train_file_ger, "gold")
        sentence_list_dev = raw.read_conll_file(raw.dev_file_ger, "gold")
        sentence_list_test = raw.read_conll_file(raw.test_file_ger, "blind")

        # create all fully connected graphs
        graph_structure.create_all_full_graphs(sentence_list_train)
        graph_structure.create_all_full_graphs(sentence_list_dev)
        graph_structure.create_all_full_graphs(sentence_list_test)

        # create all gold graphs
        graph_structure.create_all_gold_graphs(sentence_list_train)
        graph_structure.create_all_gold_graphs(sentence_list_dev)

        # extract features for training set
        print("Extracting features for training set with ", len(sentence_list_train), " sentences...")
        feat_extractor.extract_all_features(sentence_list_train, german_flag=True)
        print(str(feat_extractor.string_mapper.feat_counter - 1) + " unique features extracted.")
        save_data(file_prefix_store + ".train", sentence_list_train)

        # set freeze flag to indicate that no new features can be added anymore
        feat_extractor.string_mapper.freeze = True
        # extract features for development and test set
        print("Extracting features for development and test set...")
        feat_extractor.extract_all_features(sentence_list_dev, german_flag=True)
        save_data(file_prefix_store + ".dev", sentence_list_dev)
        feat_extractor.extract_all_features(sentence_list_test, german_flag=True)
        save_data(file_prefix_store + ".test", sentence_list_test)

        perceptron = AveragedPerceptron(feat_extractor.string_mapper.feat_counter)

    else:
        sentence_list_train = load_data(file_prefix_load + ".train")
        sentence_list_dev = load_data(file_prefix_load + ".dev")
        sentence_list_test = load_data(file_prefix_load + ".test")

        perceptron = load_weights(file_prefix_load + ".weights")
        print(str(feat_extractor.string_mapper.feat_counter - 1) + " unique features loaded.")

    perceptron.avg_train(sentence_list_train, sentence_list_dev, sentence_list_test, file_prefix_store, 500, shuffle_flag)


def english_1k_train_avg_pipeline(file_prefix_store, file_prefix_load, shuffle_flag):

    sentence_list_train = []
    sentence_list_dev = []

    feat_extractor = fe.Feature_extractor()

    perceptron = None

    if (file_prefix_load is None):
        # read necessary files
        sentence_list_train = raw.read_conll_file(raw.train_file_1k_eng, "gold")
        sentence_list_dev = raw.read_conll_file(raw.dev_file_eng, "gold")

        # create all fully connected graphs
        graph_structure.create_all_full_graphs(sentence_list_train)
        graph_structure.create_all_full_graphs(sentence_list_dev)

        # create all gold graphs
        graph_structure.create_all_gold_graphs(sentence_list_train)
        graph_structure.create_all_gold_graphs(sentence_list_dev)

        # extract features for training set
        print("Extracting features for training set with ", len(sentence_list_train), " sentences...")
        feat_extractor.extract_all_features(sentence_list_train, german_flag=False)
        print(str(feat_extractor.string_mapper.feat_counter - 1) + " unique features extracted.")
        save_data(file_prefix_store + ".train", sentence_list_train)

        # set freeze flag to indicate that no new features can be added anymore
        feat_extractor.string_mapper.freeze = True
        # extract features for development and test set
        print("Extracting features for development and test set...")
        feat_extractor.extract_all_features(sentence_list_dev, False)
        save_data(file_prefix_store + ".dev", sentence_list_dev)

        perceptron = AveragedPerceptron(feat_extractor.string_mapper.feat_counter)

    else:
        sentence_list_train = load_data(file_prefix_load + ".train")
        sentence_list_dev = load_data(file_prefix_load + ".dev")
        sentence_list_test = load_data(file_prefix_load + ".test")

        perceptron = load_weights(file_prefix_load + ".weights")
        print(str(feat_extractor.string_mapper.feat_counter - 1) + " unique features loaded.")

    perceptron.avg_train(sentence_list_train, sentence_list_dev, None, file_prefix_store, 500, shuffle_flag)


def german_1k_train_avg_pipeline(file_prefix_store, file_prefix_load, shuffle_flag):

    sentence_list_train = []
    sentence_list_dev = []

    feat_extractor = fe.Feature_extractor()

    perceptron = None

    if (file_prefix_load is None):
        # read necessary files
        sentence_list_train = raw.read_conll_file(raw.train_file_1k_ger, "gold")
        sentence_list_dev = raw.read_conll_file(raw.dev_file_ger, "gold")

        # create all fully connected graphs
        graph_structure.create_all_full_graphs(sentence_list_train)
        graph_structure.create_all_full_graphs(sentence_list_dev)

        # create all gold graphs
        graph_structure.create_all_gold_graphs(sentence_list_train)
        graph_structure.create_all_gold_graphs(sentence_list_dev)

        # extract features for training set
        print("Extracting features for training set with ", len(sentence_list_train), " sentences...")
        feat_extractor.extract_all_features(sentence_list_train, german_flag=True)
        print(str(feat_extractor.string_mapper.feat_counter - 1) + " unique features extracted.")
        save_data(file_prefix_store + ".train", sentence_list_train)

        # set freeze flag to indicate that no new features can be added anymore
        feat_extractor.string_mapper.freeze = True
        # extract features for development and test set
        print("Extracting features for development and test set...")
        feat_extractor.extract_all_features(sentence_list_dev, german_flag=True)
        save_data(file_prefix_store + ".dev", sentence_list_dev)

        perceptron = AveragedPerceptron(feat_extractor.string_mapper.feat_counter)

    else:
        sentence_list_train = load_data(file_prefix_load + ".train")
        sentence_list_dev = load_data(file_prefix_load + ".dev")

        perceptron = load_weights(file_prefix_load + ".weights")
        print(str(feat_extractor.string_mapper.feat_counter - 1) + " unique features loaded.")

    perceptron.avg_train(sentence_list_train, sentence_list_dev, None, file_prefix_store, 500, shuffle_flag)



