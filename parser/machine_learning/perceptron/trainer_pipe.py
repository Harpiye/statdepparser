from pathlib import Path
from graphs import graph_structure
from machine_learning import feature_extractor as fe
from machine_learning import feature_extractor_pipe as fep
from reader_and_writer import reader_and_writer as raw
from reader_and_writer.saver_and_loader import *
from .averaged_perceptron import AveragedPerceptron
from .structured_perceptron import Perceptron


def english_pipeline(size, file_prefix_store, file_prefix_load, shuffle_flag, avg_flag, feature_selector):

    sentence_list_train = []
    sentence_list_dev = []
    sentence_list_test = []

    feat_extractor_pipe = fep.Feature_extractor()

    perceptron = None

    if (file_prefix_load is None):
        # read necessary files
        if (size == "1h"):
            sentence_list_train = raw.read_conll_file(raw.train_file_1h_eng, "gold")
            sentence_list_dev = raw.read_conll_file(raw.dev_file_1h_eng, "gold")

        if (size == "1k"):
            # read necessary files
            sentence_list_train = raw.read_conll_file(raw.train_file_1k_eng, "gold")
            sentence_list_dev = raw.read_conll_file(raw.dev_file_eng, "gold")

        if (size == "full"):
            sentence_list_train = raw.read_conll_file(raw.train_file_eng, "gold")
            sentence_list_dev = raw.read_conll_file(raw.dev_file_eng, "gold")
            sentence_list_test = raw.read_conll_file(raw.test_file_eng, "blind")

        # create all fully connected graphs
        graph_structure.create_all_full_graphs(sentence_list_train)
        graph_structure.create_all_full_graphs(sentence_list_dev)
        if (size == "full"):
            graph_structure.create_all_full_graphs(sentence_list_test)

        # create all gold graphs
        graph_structure.create_all_gold_graphs(sentence_list_train)
        graph_structure.create_all_gold_graphs(sentence_list_dev)

        # extract features for training set
        print("Extracting features for training set with ", len(sentence_list_train), " sentences...")
        feat_extractor_pipe.extract_all_features(sentence_list_train, feature_selector, german_flag=False)
        print(str(feat_extractor_pipe.string_mapper.feat_counter - 1) + " unique features extracted.")
        #save_data("en_" + size + "_" + file_prefix_store + ".train", sentence_list_train)

        # set freeze flag to indicate that no new features can be added anymore
        feat_extractor_pipe.string_mapper.freeze = True
        # extract features for development and test set
        print("Extracting features for development and test set...")
        feat_extractor_pipe.extract_all_features(sentence_list_dev, feature_selector, False)
        #save_data("en_" + size + "_" + file_prefix_store + ".dev", sentence_list_dev)
        if (size == "full"):
            feat_extractor_pipe.extract_all_features(sentence_list_test, feature_selector, False)
            #save_data("en_" + size + "_" + file_prefix_store + ".test", sentence_list_test)

        if (avg_flag):
            perceptron = AveragedPerceptron(feat_extractor_pipe.string_mapper.feat_counter)
        else:
            perceptron = Perceptron(feat_extractor_pipe.string_mapper.feat_counter)

    else:
        sentence_list_train = load_data(file_prefix_load + ".train")
        sentence_list_dev = load_data(file_prefix_load + ".dev")
        if (size == "full"):
            sentence_list_test = load_data(file_prefix_load + ".test")

        perceptron = load_weights(file_prefix_load + ".weights")
        print(str(feat_extractor_pipe.string_mapper.feat_counter - 1) + " unique features loaded.")

    if (avg_flag):
        perceptron.avg_train(sentence_list_train, sentence_list_dev, sentence_list_test, "en_" + size + "_" + file_prefix_store, 500,
                         shuffle_flag)
    else:
        perceptron.train(sentence_list_train, sentence_list_dev, sentence_list_test, "en_" + size + "_" + file_prefix_store, 500,
                         shuffle_flag)


def german_pipeline(size, file_prefix_store, file_prefix_load, shuffle_flag, avg_flag, feature_selector):

    sentence_list_train = []
    sentence_list_dev = []
    sentence_list_test = []

    feat_extractor_pipe = fep.Feature_extractor()

    perceptron = None

    # read necessary files
    if (size == "1h"):
        sentence_list_train = raw.read_conll_file(raw.train_file_1h_ger, "gold")
        sentence_list_dev = raw.read_conll_file(raw.dev_file_1h_ger, "gold")

    if (size == "1k"):
        # read necessary files
        sentence_list_train = raw.read_conll_file(raw.train_file_1k_ger, "gold")
        sentence_list_dev = raw.read_conll_file(raw.dev_file_ger, "gold")

    if (size == "full"):
        sentence_list_train = raw.read_conll_file(raw.train_file_ger, "gold")
        sentence_list_dev = raw.read_conll_file(raw.dev_file_ger, "gold")
        sentence_list_test = raw.read_conll_file(raw.test_file_ger, "blind")

    # create all fully connected graphs
    graph_structure.create_all_full_graphs(sentence_list_train)
    graph_structure.create_all_full_graphs(sentence_list_dev)
    if (size == "full"):
        graph_structure.create_all_full_graphs(sentence_list_test)

    # create all gold graphs
    graph_structure.create_all_gold_graphs(sentence_list_train)
    graph_structure.create_all_gold_graphs(sentence_list_dev)

    # extract features for training set
    print("Extracting features for training set with ", len(sentence_list_train), " sentences...")
    feat_extractor_pipe.extract_all_features(sentence_list_train, feature_selector, german_flag=True)
    print(str(feat_extractor_pipe.string_mapper.feat_counter - 1) + " unique features extracted.")
    #save_data("de_" + size + "_" + file_prefix_store + ".train", sentence_list_train)

    # set freeze flag to indicate that no new features can be added anymore
    feat_extractor_pipe.string_mapper.freeze = True
    # extract features for development and test set
    print("Extracting features for development and test set...")
    feat_extractor_pipe.extract_all_features(sentence_list_dev, feature_selector, german_flag=True)
    #save_data("de_" + size + "_" + file_prefix_store + ".dev", sentence_list_dev)
    if (size == "full"):
        feat_extractor_pipe.extract_all_features(sentence_list_test, feature_selector, german_flag=True)
        #save_data("de_" + size + "_" + file_prefix_store + ".test", sentence_list_test)

    if (file_prefix_load is None):
        if (avg_flag):
            perceptron = AveragedPerceptron(feat_extractor_pipe.string_mapper.feat_counter)
        else:
            perceptron = Perceptron(feat_extractor_pipe.string_mapper.feat_counter)

    else:
        # sentence_list_train = load_data(file_prefix_load + ".train")
        # sentence_list_dev = load_data(file_prefix_load + ".dev")
        # if (size == "full"):
        #     sentence_list_test = load_data(file_prefix_load + ".test")

        perceptron = load_weights(file_prefix_load + ".weights")
        print(str(feat_extractor_pipe.string_mapper.feat_counter - 1) + " unique features loaded.")

    if (avg_flag):
        perceptron.avg_train(sentence_list_train, sentence_list_dev, sentence_list_test, "de_" + size + "_" + file_prefix_store, 500,
                         shuffle_flag)
    else:
        perceptron.train(sentence_list_train, sentence_list_dev, sentence_list_test, "de_" + size + "_" + file_prefix_store, 500,
                         shuffle_flag)


# def german_pipeline(size, file_prefix_store, file_prefix_load, shuffle_flag, avg_flag, feature_selector):
#
#     sentence_list_train = []
#     sentence_list_dev = []
#     sentence_list_test = []
#
#     feat_extractor_pipe = fep.Feature_extractor()
#
#     perceptron = None
#
#     if (file_prefix_load is None):
#         # read necessary files
#         if (size == "1h"):
#             sentence_list_train = raw.read_conll_file(raw.train_file_1h_ger, "gold")
#             sentence_list_dev = raw.read_conll_file(raw.dev_file_1h_ger, "gold")
#
#         if (size == "1k"):
#             # read necessary files
#             sentence_list_train = raw.read_conll_file(raw.train_file_1k_ger, "gold")
#             sentence_list_dev = raw.read_conll_file(raw.dev_file_ger, "gold")
#
#         if (size == "full"):
#             sentence_list_train = raw.read_conll_file(raw.train_file_ger, "gold")
#             sentence_list_dev = raw.read_conll_file(raw.dev_file_ger, "gold")
#             sentence_list_test = raw.read_conll_file(raw.test_file_ger, "blind")
#
#         # create all fully connected graphs
#         graph_structure.create_all_full_graphs(sentence_list_train)
#         graph_structure.create_all_full_graphs(sentence_list_dev)
#         if (size == "full"):
#             graph_structure.create_all_full_graphs(sentence_list_test)
#
#         # create all gold graphs
#         graph_structure.create_all_gold_graphs(sentence_list_train)
#         graph_structure.create_all_gold_graphs(sentence_list_dev)
#
#         # extract features for training set
#         print("Extracting features for training set with ", len(sentence_list_train), " sentences...")
#         feat_extractor_pipe.extract_all_features(sentence_list_train, feature_selector, german_flag=True)
#         print(str(feat_extractor_pipe.string_mapper.feat_counter - 1) + " unique features extracted.")
#         #save_data("de_" + size + "_" + file_prefix_store + ".train", sentence_list_train)
#
#         # set freeze flag to indicate that no new features can be added anymore
#         feat_extractor_pipe.string_mapper.freeze = True
#         # extract features for development and test set
#         print("Extracting features for development and test set...")
#         feat_extractor_pipe.extract_all_features(sentence_list_dev, feature_selector, german_flag=True)
#         #save_data("de_" + size + "_" + file_prefix_store + ".dev", sentence_list_dev)
#         if (size == "full"):
#             feat_extractor_pipe.extract_all_features(sentence_list_test, feature_selector, german_flag=True)
#             #save_data("de_" + size + "_" + file_prefix_store + ".test", sentence_list_test)
#
#         if (avg_flag):
#             perceptron = AveragedPerceptron(feat_extractor_pipe.string_mapper.feat_counter)
#         else:
#             perceptron = Perceptron(feat_extractor_pipe.string_mapper.feat_counter)
#
#     else:
#         sentence_list_train = load_data(file_prefix_load + ".train")
#         sentence_list_dev = load_data(file_prefix_load + ".dev")
#         if (size == "full"):
#             sentence_list_test = load_data(file_prefix_load + ".test")
#
#         perceptron = load_weights(file_prefix_load + ".weights")
#         print(str(feat_extractor_pipe.string_mapper.feat_counter - 1) + " unique features loaded.")
#
#     if (avg_flag):
#         perceptron.avg_train(sentence_list_train, sentence_list_dev, sentence_list_test, "de_" + size + "_" + file_prefix_store, 500,
#                          shuffle_flag)
#     else:
#         perceptron.train(sentence_list_train, sentence_list_dev, sentence_list_test, "de_" + size + "_" + file_prefix_store, 500,
#                          shuffle_flag)