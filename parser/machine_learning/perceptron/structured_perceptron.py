import numpy as np
from decoding import chu_liu_edmonds as cle
from random import shuffle
from evaluation import attachment_score as eval
from reader_and_writer import reader_and_writer as raw
from pathlib import Path
from termcolor import cprint
from reader_and_writer.saver_and_loader import save_weights
from machine_learning.perceptron.trainer import pred_path


class Perceptron:

    def __init__(self, unique_feat_nr):
        # initialize numpy vector of length unique_feat_nr with zeros
        self.weight_vec = np.zeros(unique_feat_nr, dtype=int)


    def train(self, train_data, dev_data, test_data, file_prefix, iter_nr, shuffle_flag):
        print("Training average perceptron...")

        # number of training iterations with no improvement after which training will be stopped
        patience = 10
        patience_count = patience

        curr_eval_score = 0 # the last calculated score on the development set
        best_eval_score = 0 # the best achieved score on the development set so far
        best_eval_iter = 0 # and the iteration number of this score

        curr_train_score = 0 # the last calculated score on the training set
        best_train_score = 0 # best score on the training set so far
        best_train_iter = 0 # and the iteration number of this score

        pred_file = file_prefix + ".conll.pred"

        for i in range(1, iter_nr+1):

            patience_count -= 1
            if (patience_count >= 0):

                #shuffle(train_data)
                for train_instance in train_data:

                    self.calculate_arc_scores(train_instance)

                    train_instance.pred_graph = cle.chu_liu_edmonds_algorithm(train_instance.full_graph)

                    if train_instance.pred_graph.arcs_feat!=train_instance.gold_graph.arcs_feat:
                        self.update_weights(train_instance)

                    self.save_pred_tokens(train_instance)

                res_train = eval.evaluate_list(train_data)
                # raw.write_pred_conll_file(raw.dev_path_eng / "wsj_dev.conll06.pred", dev_data)
                res_dev = self.predict_and_evaluate_dev(dev_data)
                curr_train_score = res_train[2]
                curr_eval_score = res_dev[2]

                cprint((str(i) + ". iteration: UAS on train: " + str(res_train[1]) + "/" + str(res_train[0]) + " = " + str(res_train[2])
                      + "%  -  UAS on dev: " + str(res_dev[1]) + "/" + str(res_dev[0]) + " = " + str(res_dev[2]) + "%"), 'white')

                if (curr_eval_score > best_eval_score):
                    best_eval_score = curr_eval_score
                    best_eval_iter = i
                    patience_count = patience

                    if test_data is not None:
                        pred_file = file_prefix + "_i" + str(i) + ".conll.pred"
                        self.predict_and_write_test(test_data, pred_path / pred_file)

                    save_weights(file_prefix + ".weights", self)

                if (curr_train_score > best_train_score):
                    best_train_score = curr_train_score
                    best_train_iter = i

                if shuffle_flag:
                    shuffle(train_data)

            # leave the training and development phase if the score hasn't increased during the last iterations
            else:
                break

        print("\nCurrent UAS on the training set is ", curr_train_score, "%.")
        print("Best UAS was ", best_train_score, "% with ", best_train_iter, " iterations.")

        print("Current UAS on the development set is ", curr_eval_score, "%.")
        print("Best UAS was ", best_eval_score, "% with ", best_eval_iter, " iterations.")


    def calculate_arc_scores(self, instance):
        for arc, feat_list in instance.full_graph.arcs_feat.items():
            arc_score = np.sum(np.asarray(self.weight_vec[feat_list], dtype=int))
            instance.full_graph.update_arc(arc[0], arc[1], arc_score)


    def update_weights(self, instance):
        for pred_arc in instance.pred_graph.arcs_feat.keys():
            for fi in instance.full_graph.arcs_feat[pred_arc]:
                self.weight_vec[fi] -= 1
        for gold_arc in instance.gold_graph.arcs_feat.keys():
            for fi in instance.full_graph.arcs_feat[gold_arc]:
                self.weight_vec[fi] += 1


    def save_pred_tokens(self, instance):
        for node_nr, node in instance.pred_graph.nodes_dict.items():
            instance.token_list[node_nr].pred_head = next(iter(node.heads.keys()), "_")


    def predict_and_evaluate_dev(self, dev_data):
        inst = 0
        for dev_instance in dev_data:
            inst += 1
            self.calculate_arc_scores(dev_instance)
            dev_instance.pred_graph = cle.chu_liu_edmonds_algorithm(dev_instance.full_graph)
            self.save_pred_tokens(dev_instance)

        res = eval.evaluate_list(dev_data) # returns: token_count, correct_head_count, UAS, correct_head_and_label_count, LAS
        return res


    def predict_and_write_test(self, test_data, output_path):
        for test_instance in test_data:
            self.calculate_arc_scores(test_instance)
            test_instance.pred_graph = cle.chu_liu_edmonds_algorithm(test_instance.full_graph)
            self.save_pred_tokens(test_instance)
        raw.write_pred_conll_file(output_path, test_data)





