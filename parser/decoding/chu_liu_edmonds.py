from graphs import graph_structure, drawing
import copy
import pickle
# TODO: remove termcolor from all scripts
from termcolor import colored, cprint


def chu_liu_edmonds_algorithm(curr_graph):
    """
    Chu-Liu-Edmonds algorithm for finding the maximum spanning tree for a given graph.

    :param curr_graph: graph
    :return: maximum spanning tree
    """

    # first iteration: curr_graph is a fully connected graph

    reduced_graph = find_max_heads(curr_graph) # for each token, take the highest-scoring head

    cycle_graph = find_next_cycle(reduced_graph)

    if cycle_graph == None: # finished with no cycles
        return reduced_graph
    else:
        contracted_graph = contract(curr_graph, cycle_graph)

        # selects best-scoring heads, checks for more cycles
        resulting_graph = chu_liu_edmonds_algorithm(contracted_graph) # returns reduced graph

        resolved_graph = resolve(resulting_graph, cycle_graph) # expanding cycle

    return resolved_graph


def find_max_heads(curr_graph):
    """
    For each node, finds the highest-scoring head.

    :param curr_graph: Initial graph in the first iteration, graph with a contracted cycle node
                       in later iterations
    :return: reduced graph with only the arcs that belong to a node and its highest-scoring head
    """
    reduced_graph = graph_structure.Graph()
    for each_node in curr_graph.nodes_dict.values():
        if each_node.is_cycle_node == True:
            reduced_graph.add_contracted_node(each_node.node_nr, each_node.node_value, each_node.cycle_arcs_dict,
                                              each_node.original_dependents, each_node.original_heads)
        else:
            reduced_graph.add_node(each_node.node_nr, each_node.node_value)
    for each_node in curr_graph.nodes_dict.values():
        if len(each_node.heads) > 0: # root has no heads
            a = each_node.node_nr
            b = each_node.heads
            max_key = max(each_node.heads, key=lambda key: each_node.heads[key])
            reduced_graph.add_arc(max_key, each_node.node_nr, each_node.heads[max_key])
    return reduced_graph


def find_next_cycle(reduced_graph):
    """
    Checks the graph for cycles.

    :param reduced_graph: graph only with the arcs that belong to a best-scoring head
    :return: None
    """
    cycle = graph_structure.Graph()

    visited = [False] * len(reduced_graph.nodes_dict)  # list to check whether we've already visited a node
    rec_stack = [False] * len(reduced_graph.nodes_dict)  # recursion stack to check if the current node was already seen on this path
    path = []
    for each_node in reduced_graph.nodes_dict.keys():  # for each Node
        if visited[each_node] == False:
            if is_part_of_cycle(reduced_graph, reduced_graph.nodes_dict[each_node], visited, rec_stack, path) == True:
                # generate cycle_graph from the path list
                # add first node since in the loop only the next node will be added
                cycle.add_node(path[0], reduced_graph.nodes_dict[path[0]])
                for i in range(0, len(path)-1):
                    curr_node_nr = path[i]
                    next_node_nr = path[i+1]
                    # next node always needs to be added before the arc can be added
                    cycle.add_node(next_node_nr, reduced_graph.nodes_dict[next_node_nr])
                    cycle.add_arc(curr_node_nr, next_node_nr, reduced_graph.nodes_dict[curr_node_nr].dependents[next_node_nr])
                # add the arc from the last node to the first node so that the cycle is complete
                last_node_nr = path[len(path)-1]
                cycle.add_arc(last_node_nr, path[0], reduced_graph.nodes_dict[last_node_nr].dependents[path[0]])
                # append contracted node to the graph with the last index increased by one
                cycle.set_cycle_node_nr(len(reduced_graph.nodes_dict))
                return cycle
    return None


def is_part_of_cycle(graph, curr_node, visited, rec_stack, path):
    """
    Checks whether a given node within a graph is part of a cycle.

    :param graph: graph containing the node
    :param curr_node: the node to be checked
    :param visited: list of booleans to check whether we've already visited a node
    :param rec_stack: recursion stack (also list of booleans) to check if the current node was already seen on this path
    :param path: list of the node numbers we've already visited
    :return:
    """

    visited[curr_node.node_nr] = True # mark current node as visited
    rec_stack[curr_node.node_nr] = True # and add to recursion stack
    path.append(curr_node.node_nr)

    for each_dep in curr_node.dependents: # for each dependent of the current node
        if visited[each_dep] == False:
            if is_part_of_cycle(graph, graph.nodes_dict[each_dep], visited, rec_stack, path) == True:
                return True
        elif rec_stack[each_dep] == True: # found currenty visited node in recursion stack -> cycle detected!
            return True

    # pop node from stack when no cycle was detected
    rec_stack[curr_node.node_nr] = False
    path.remove(curr_node.node_nr)
    return False


def contract(curr_graph, curr_cycle):
    """
    Contract a cycle in a graph to a new node.

    :param curr_graph: graph containing the cycle
    :param curr_cycle: sub-graph containing the cycle
    :return: the new graph with the contracted node
    """
    #derived_graph = copy.deepcopy(curr_graph)
    derived_graph = pickle.loads(pickle.dumps(curr_graph))

    # 1. contract cycles node to one node
    cycle_id = curr_cycle.cycle_node_nr
    # initial graph with additional empty node for the cycle:
    derived_graph.add_contracted_node(cycle_id, None, curr_cycle.arcs_dict)

    cycle_score = 0
    for node, arcs in curr_cycle.arcs_dict.items():
        for dep, score in arcs.items():
            cycle_score += score
    # 2. update node references
    # iterate over old graph
    for each_head, dep_dict in curr_graph.arcs_dict.items():
        for each_dep, arc_score in dep_dict.items():
            #if (each_head==6) & (each_dep==8):
            #    print("6->8")
            if each_head in curr_cycle.arcs_dict.keys(): # head node is in cycle
                if each_dep in curr_cycle.arcs_dict.keys(): # dep node is in cycle -> arc within cycle
                    derived_graph.del_arc(each_head, each_dep)
                    # add nothing because we already have this information stored in cycle_arcs_dict for the contracted node
                else: # dep node is not in cycle -> arc leaving cycle
                    # just keep the arc with the highest score leaving the cycle for each dependent,
                    # i.e. for all nodes in the cycle and this dependent, only keep the highest
                    to_comp = derived_graph.arcs_dict[cycle_id]
                    #if derived_graph.arcs_dict[cycle_id] != {}: # just if there's a value to compare with
                    if each_dep in derived_graph.arcs_dict[cycle_id]: # just if there's a value to compare with
                        # if next(iter(derived_graph.arcs_dict[cycle_id].values())) == arc_score:
                        #     cprint('same1!!!!!!!', 'magenta')
                        # TODO: decide what to do if both have the same score
                        if derived_graph.arcs_dict[cycle_id][each_dep] < arc_score: # and it's higher
                            #derived_graph.arcs_dict[cycle_id].clear()
                            #derived_graph.add_arc(cycle_id, each_dep, arc_score)
                            derived_graph.update_arc(cycle_id, each_dep, arc_score)
                            derived_graph.nodes_dict[cycle_id].original_dependents[(each_dep, arc_score)] = each_head
                            # just overwrite the current value as we're only interested in the highest one
                            # remember {original_head (in cycle) : arc_score} portion of {original_head: {each_dep, arc_score}
                    else: # no value to compare with -> add arc to contracted graph
                        derived_graph.add_arc(cycle_id, each_dep, arc_score)
                        derived_graph.nodes_dict[cycle_id].original_dependents[(each_dep, arc_score)] = each_head
                    derived_graph.del_arc(each_head, each_dep)
            else: # head node is not in cycle
                if each_dep in curr_cycle.arcs_dict.keys():  # but dep node is in cycle -> arc entering cycle
                    # 3. recompute scores
                    new_score = arc_score + cycle_score - next(iter(curr_cycle.nodes_dict[each_dep].heads.values()))
                    if each_head in derived_graph.nodes_dict[cycle_id].heads.keys(): # just if there's already an arc entering the cycle from this head
                        # if derived_graph.nodes_dict[cycle_id].heads[each_head] == new_score:
                            # TODO: decide what to do if both have the same score
                        if derived_graph.nodes_dict[cycle_id].heads[each_head] < new_score:  # and it's higher
                            derived_graph.add_arc(each_head, cycle_id, new_score)
                            # remember {original_dep (in cycle) : arc_score} portion of {each_head: {original_dep, arc_score}
                            derived_graph.nodes_dict[cycle_id].original_heads[(each_head, new_score)] = (each_dep, arc_score)
                    else:
                        derived_graph.add_arc(each_head, cycle_id, new_score)
                        derived_graph.nodes_dict[cycle_id].original_heads[(each_head, new_score)] = (each_dep, arc_score)
                    derived_graph.del_arc(each_head, each_dep)
                else: # and dep node is neither -> arc outside of cycle
                    pass # arc doesn't need to be changed
    return derived_graph


def resolve(curr_graph, curr_cycle):
    """
    Resolve an earlier contracted cycle.

    :param curr_graph: graph containing the contracted cycle
    :param curr_cycle: subgraph containing the contracted cycle
    :return: graph including the resolved cycle
    """
    cycle_node = curr_graph.nodes_dict[curr_cycle.cycle_node_nr]
    if cycle_node.is_cycle_node == True:
        # iterator for the dependents of the contracted cycle node
        curr_dependent = next(iter(cycle_node.dependents.keys()), None)
        while curr_dependent is not None:
            curr_dep_score = cycle_node.dependents[curr_dependent]
            orig_node_for_dep = cycle_node.original_dependents[(curr_dependent, curr_dep_score)]
            # change arc leaving cycle
            curr_graph.add_arc(orig_node_for_dep, curr_dependent, curr_dep_score)
            curr_graph.del_arc(cycle_node.node_nr, curr_dependent)
            curr_dependent = next(iter(cycle_node.dependents.keys()), None)


        curr_head = next(iter(cycle_node.heads.keys()), None)
        while curr_head is not None:
            curr_head_score = cycle_node.heads[curr_head]
            orig_tuple_for_head = cycle_node.original_heads[(curr_head, curr_head_score)]
            # change arc entering cycle
            curr_graph.add_arc(curr_head, orig_tuple_for_head[0], orig_tuple_for_head[1])
            curr_graph.del_arc(curr_head, cycle_node.node_nr)
            curr_head = next(iter(cycle_node.heads.keys()), None)

        # add arc(s) between (original) cycle nodes: for each original cycle node
        for cycle_node_nr, cycle_deps in cycle_node.cycle_arcs_dict.items():
            for this_cycle_node, arc_score in cycle_deps.items():
                # if it hasn't a head yet (i.e.  it's not entrance point of the cycle)
                if curr_graph.nodes_dict[this_cycle_node].heads == {}:
                    curr_graph.add_arc(cycle_node_nr, this_cycle_node, arc_score)

        # delete contracted node
        curr_graph.del_node(cycle_node.node_nr)

    return curr_graph
