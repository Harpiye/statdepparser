from prettytable import PrettyTable
from random import randint
import numpy

class Node:

    def __init__(self, node_nr, node_value):
        self.node_nr = node_nr # id
        self.node_value = node_value # string value
        self.dependents = {} # key: dependent_nr, value: score
        self.heads = {} # key: head_nr, value: score
        self.is_cycle_node = False


    def add_dependent(self, neighbor_nr, score):
        """only to be called within methods of the graph class"""
        self.dependents[neighbor_nr] = score

    def add_head(self, neighbor_nr, score):
        """only to be called within methods of the graph class"""
        self.heads[neighbor_nr] = score

    def del_dependent(self, neighbor_nr):
        """only to be called within methods of the graph class"""
        self.dependents.pop(neighbor_nr)

    def del_head(self, neighbor_nr):
        """only to be called within methods of the graph class"""
        self.heads.pop(neighbor_nr)

    def get_dependent_connections(self):
        return self.dependents.keys()

    def get_head_connections(self):
        return self.heads.keys()

    def get_id(self):
        return self.node_nr

    def get_dependent_score(self, neighbor_nr):
        return self.dependents[neighbor_nr]

    def get_head_score(self, neighbor_nr):
        return self.heads[neighbor_nr]


class Contracted_node(Node):

    def __init__(self, node_nr, node_value, cycle_arcs_dict, original_dependents=None, original_heads=None):
        Node.__init__(self, node_nr, node_value)
        self.node_value = "" if node_value is None else node_value
        self.dependents = {} # self -> dep
        self.heads = {} # head -> self
        self.is_cycle_node = True
        self.cycle_arcs_dict = cycle_arcs_dict
        # {(self_resolved, dependent): score} for arcs leaving the cycle:
        self.original_dependents = {} if original_dependents is None else original_dependents
        # {(head, self_resolved): (updated_score, original_score)} for arcs entering the cycle:
        self.original_heads = {} if original_heads is None else original_heads


class Graph:

    def __init__(self, nodes_dict=None, arcs_dict=None, arcs_feat=None, table=None, cycle_node_nr=None):
        self.nodes_dict = {} if nodes_dict is None else nodes_dict
        # nested dictionary: {head: {dep: score}}
        self.arcs_dict = {} if arcs_dict is None else arcs_dict
        # key: (head, dep) tuple; value: feat_vector [] with indices of features for each arc
        self.arcs_feat = {} if arcs_feat is None else arcs_feat
        self.table = None if table is None else table
        # if the graph is a cycle_graph that's supposed to be contracted then this shows the number of the contracted node
        self.cycle_node_nr = -1 if cycle_node_nr is None else cycle_node_nr


    def get_node_value(self, node_nr):
        node_value = self.nodes_dict[node_nr].node_value
        return node_value


    def add_node(self, node_nr, node_value):
        new_node = Node(node_nr, node_value)
        self.nodes_dict[node_nr] = new_node
        self.arcs_dict[node_nr] = {}
        return new_node


    def del_node(self, node_nr):
        if self.arcs_dict[node_nr] == {}:
            self.arcs_dict.pop(node_nr)
            self.nodes_dict.pop(node_nr)


    def add_contracted_node(self, node_nr, node_value, cycle_arcs_dict, original_dependents=None, original_heads=None):
        new_node = Contracted_node(node_nr, node_value, cycle_arcs_dict, original_dependents, original_heads)
        self.nodes_dict[node_nr] = new_node
        self.arcs_dict[node_nr] = {}
        return new_node


    def add_arc(self, from_node_nr, to_node_nr, score):
        """premise: all nodes are already part of the nodes_list and a key entry in the arcs_dict"""
        self.arcs_dict[from_node_nr].update({to_node_nr: score})
        self.arcs_feat[(from_node_nr, to_node_nr)] = []
        self.nodes_dict[from_node_nr].add_dependent(to_node_nr, score)
        self.nodes_dict[to_node_nr].add_head(from_node_nr, score)
        wasAdded = True
        return wasAdded


    def del_arc(self, from_node_nr, to_node_nr):
        if from_node_nr in self.arcs_dict.keys():
            self.arcs_dict[from_node_nr].pop(to_node_nr)
            self.arcs_feat.pop((from_node_nr, to_node_nr))
            self.nodes_dict[from_node_nr].del_dependent(to_node_nr)
            self.nodes_dict[to_node_nr].del_head(from_node_nr)


    def update_arc(self, from_node_nr, to_node_nr, new_score):
        self.arcs_dict[from_node_nr].update({to_node_nr: new_score})
        self.nodes_dict[from_node_nr].dependents[to_node_nr] = new_score
        self.nodes_dict[to_node_nr].heads[from_node_nr] = new_score


    def set_cycle_node_nr(self, cycle_node_id):
        self.cycle_node_nr = cycle_node_id


def print_table(graph):
    field_names = []
    graph.table = PrettyTable()
    for eachNode in graph.nodes_dict.values():
        dep_list = []
        field_names.append(eachNode.node_value)
        # eachDep is a (dependent: score) dictionary pair
        for i in range(0, len(graph.nodes_dict), 1):
            if i in eachNode.dependents:
                dep_list.append(eachNode.dependents[i])
            else:
                dep_list.append(numpy.inf)
        graph.table.add_row(dep_list)
    graph.table.field_names = field_names


def create_graph(sentence):
    graph = Graph()
    for eachToken in sentence.token_list:
        graph.add_node(eachToken.id, eachToken.form)
    for eachToken in sentence.token_list:
        graph.add_arc(eachToken.head, eachToken.id, randint(0, 30))
    return graph


def create_graph_from_dict(arcs_dict):
    graph = Graph()
    for node in arcs_dict.keys():
        graph.add_node(node, None)
    for head, dep_dict in arcs_dict.items():
        for dep, score in dep_dict.items():
            graph.add_arc(head, dep, score)
    return graph


def create_graph_with_all_arcs_randomly(sentence):
    graph = Graph()
    for each_token in sentence.token_list:
        graph.add_node(each_token.id, each_token.form)
    for each_token in sentence.token_list:
        # arcs for all other nodes
        iter_nodes = iter(graph.nodes_dict.values())
        next(iter_nodes)
        for each_node in iter_nodes:
            if each_node.node_nr != each_token.id:
                graph.add_arc(each_token.id, each_node.node_nr, randint(0, 30))
                graph.arcs_feat[(each_token.id, each_node.node_nr)] = []
    sentence.full_graph = graph
    return graph


def create_graph_with_all_arcs_correctly(sentence):
    graph = Graph()
    # iterate once over all tokens to add them as nodes
    for each_token in sentence.token_list:
        graph.add_node(each_token.id, each_token.form)

    # iterate again over all tokens to add for each dependent all possible heads - 0 has no head
    for i in range(1, len(sentence.token_list)):
        # create the scores in such a way that correct arcs have higher scores than others:
        # - gold heads get a value between 21 and 30
        # - all the others get a value between 0 and 20
        gold_head = sentence.token_list[i].gold_head
        # add all arcs to this dependent
        iter_nodes = iter(graph.nodes_dict.values())
        for each_node in iter_nodes:
            if gold_head == each_node.node_nr:
                graph.add_arc(each_node.node_nr, sentence.token_list[i].id, randint(21, 30))
            elif each_node.node_nr == sentence.token_list[i].id:
                pass # no nodes to itself
            else:
                graph.add_arc(each_node.node_nr, sentence.token_list[i].id, randint(0, 20))
    sentence.full_graph = graph
    return graph


def create_gold_graph(sentence):
    graph = Graph()
    # iterate once over all tokens to add them as nodes
    for each_token in sentence.token_list:
        graph.add_node(each_token.id, each_token.form)
    for i in range(1, len(sentence.token_list)):
        graph.add_arc(sentence.token_list[i].gold_head, sentence.token_list[i].id, -1)

    sentence.gold_graph = graph
    return graph


def create_graph_with_all_arcs_without_scores(sentence):
    graph = Graph()
    # iterate once over all tokens to add them as nodes
    for each_token in sentence.token_list:
        graph.add_node(each_token.id, each_token.form)

    # iterate again over all tokens to add for each dependent all possible heads - 0 has no head
    for i in range(1, len(sentence.token_list)):
        # create the scores in such a way that correct arcs have higher scores than others:
        # - gold heads get a value between 21 and 30
        # - all the others get a value between 0 and 20
        gold_head = sentence.token_list[i].gold_head
        # add all arcs to this dependent
        iter_nodes = iter(graph.nodes_dict.values())
        for each_node in iter_nodes:
            if gold_head == each_node.node_nr:
                graph.add_arc(each_node.node_nr, sentence.token_list[i].id, -1)
            elif each_node.node_nr == sentence.token_list[i].id:
                pass # no nodes to itself
            else:
                graph.add_arc(each_node.node_nr, sentence.token_list[i].id, -1)
    sentence.full_graph = graph
    return graph


def create_all_full_graphs(sentence_list):
    for sentence in sentence_list:
        create_graph_with_all_arcs_without_scores(sentence)


def create_all_gold_graphs(sentence_list):
    for sentence in sentence_list:
        create_gold_graph(sentence)