from graphs import graph_structure
from graphviz import Graph, Digraph


def create_ex_graph_1():
    # fully connected graphs from 02_graph_parsing.pdf, page 51
    ex_graph = graph_structure.Graph()

    ex_graph.add_node(0, "ROOT")
    ex_graph.add_node(1, "John")
    ex_graph.add_node(2, "saw")
    ex_graph.add_node(3, "Mary")

    ex_graph.add_arc(0, 1, 9)
    ex_graph.add_arc(0, 2, 10)
    ex_graph.add_arc(0, 3, 9)
    ex_graph.add_arc(1, 2, 20)
    ex_graph.add_arc(1, 3, 3)
    ex_graph.add_arc(2, 1, 30)
    ex_graph.add_arc(2, 3, 30)
    ex_graph.add_arc(3, 1, 11)
    ex_graph.add_arc(3, 2, 0)

    return ex_graph

def create_final_ex_graph_1():
    ex_graph = graph_structure.Graph()

    ex_graph.add_node(0, "ROOT")
    ex_graph.add_node(1, "John")
    ex_graph.add_node(2, "saw")
    ex_graph.add_node(3, "Mary")

    ex_graph.add_arc(0, 2, 10)
    ex_graph.add_arc(2, 1, 30)
    ex_graph.add_arc(2, 3, 30)

    return ex_graph


def create_ex_graph_2():
    # fully connected graphs from 02_graph_parsing.pdf, page 26
    ex_graph = graph_structure.Graph()

    ex_graph.add_node(0, "ROOT")
    ex_graph.add_node(1, "a")
    ex_graph.add_node(2, "read")
    ex_graph.add_node(3, "book")

    ex_graph.add_arc(0, 1, 3)
    ex_graph.add_arc(0, 2, 10)
    ex_graph.add_arc(0, 3, 5)
    ex_graph.add_arc(1, 2, 1)
    ex_graph.add_arc(1, 3, 10)
    ex_graph.add_arc(2, 1, 10)
    ex_graph.add_arc(2, 3, 8)
    ex_graph.add_arc(3, 1, 20)
    ex_graph.add_arc(3, 2, 5)

    return ex_graph


def create_final_ex_graph_2():
    # fully connected graphs from 02_graph_parsing.pdf, page 26
    ex_graph = graph_structure.Graph()

    ex_graph.add_node(0, "ROOT")
    ex_graph.add_node(1, "a")
    ex_graph.add_node(2, "read")
    ex_graph.add_node(3, "book")

    ex_graph.add_arc(0, 2, 10)
    ex_graph.add_arc(2, 3, 8)
    ex_graph.add_arc(3, 1, 20)

    return ex_graph


def create_ex_graph_3():
    # fully connected graphs from 02_graph_parsing.pdf, page 26
    ex_graph = graph_structure.Graph()

    ex_graph.add_node(0, "ROOT")
    ex_graph.add_node(1, "I")
    ex_graph.add_node(2, "read")
    ex_graph.add_node(3, "a")
    ex_graph.add_node(4, "book")

    ex_graph.add_arc(0, 1, 12)
    ex_graph.add_arc(0, 2, 4)
    ex_graph.add_arc(0, 3, 2)
    ex_graph.add_arc(0, 4, 9)
    ex_graph.add_arc(1, 2, 2)
    ex_graph.add_arc(1, 3, 6)
    ex_graph.add_arc(1, 4, 1)
    ex_graph.add_arc(2, 1, 6)
    ex_graph.add_arc(2, 3, 8)
    ex_graph.add_arc(2, 4, 15)
    ex_graph.add_arc(3, 1, 8)
    ex_graph.add_arc(3, 2, 3)
    ex_graph.add_arc(3, 4, 17)
    ex_graph.add_arc(4, 1, 10)
    ex_graph.add_arc(4, 2, 9)
    ex_graph.add_arc(4, 3, 5)
    return ex_graph


def create_final_ex_graph_3():
    # fully connected graphs from 02_graph_parsing.pdf, page 26
    ex_graph = graph_structure.Graph()

    ex_graph.add_node(0, "ROOT")
    ex_graph.add_node(1, "I")
    ex_graph.add_node(2, "read")
    ex_graph.add_node(3, "a")
    ex_graph.add_node(4, "book")

    ex_graph.add_arc(0, 1, 12)
    ex_graph.add_arc(1, 3, 6)
    ex_graph.add_arc(3, 4, 17)
    ex_graph.add_arc(4, 2, 9)
    return ex_graph


def create_ex_graph_4():
    # example from the slides at https://courses.cs.washington.edu/courses/cse490u/17wi/slides/CLE.pdf
    ex_graph = graph_structure.Graph()

    ex_graph.add_node(0, "ROOT")
    ex_graph.add_node(1, "V1")
    ex_graph.add_node(2, "V2")
    ex_graph.add_node(3, "V3")

    ex_graph.add_arc(0, 1, 5)
    ex_graph.add_arc(0, 2, 1)
    ex_graph.add_arc(0, 3, 1)
    ex_graph.add_arc(1, 2, 11)
    ex_graph.add_arc(1, 3, 4)
    ex_graph.add_arc(2, 1, 10)
    ex_graph.add_arc(2, 3, 5)
    ex_graph.add_arc(3, 1, 9)
    ex_graph.add_arc(3, 2, 8)
    return ex_graph

def create_final_ex_graph_4():
    ex_graph = graph_structure.Graph()

    ex_graph.add_node(0, "ROOT")
    ex_graph.add_node(1, "V1")
    ex_graph.add_node(2, "V2")
    ex_graph.add_node(3, "V3")

    ex_graph.add_arc(0, 1, 5)
    ex_graph.add_arc(1, 2, 11)
    ex_graph.add_arc(2, 3, 5)

    return ex_graph


def draw_ex_graph():
    g = Digraph('example_graph', filename = 'ex_graph.gv')
    g.attr(rankdir='LR')

    g.attr('node', shape='circle')
    g.edge('ROOT', 'John', label='9')
    g.edge('ROOT', 'saw', label='10')
    g.edge('ROOT', 'Mary', label='9')
    g.edge('John', 'saw', label='20')
    g.edge('John', 'Mary', label='3')
    g.edge('saw', 'John', label='30')
    g.edge('saw', 'Mary', label='30')
    g.edge('Mary', 'John', label='11')
    g.edge('Mary', 'saw', label='0')

    g.view()