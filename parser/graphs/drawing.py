from pathlib import Path

from graphs import graph_structure
import networkx
import numpy
import pydot
from matplotlib import pyplot
from networkx.drawing.nx_pydot import graphviz_layout
from graphviz import Graph, Digraph


def draw_graph(graph):
    "Draw the input graph."
    storage_path = Path.cwd() / 'output' / 'graph_drawings'
    g = Digraph('current_graph', filename=storage_path/'current_graph.gv')
    g.attr(rankdir='LR')

    g.attr('node', shape='circle')
    for each_node, neighbors in graph.arcs_dict.items():
        for each_neighbor_node, score in neighbors.items():
            g.edge(str(each_node), str(each_neighbor_node), str(score))

    g.view()