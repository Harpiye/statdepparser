from machine_learning.perceptron import trainer
from sty import fg, bg, ef, rs
from reader_and_writer import reader_and_writer as raw


def main():

    trainer.english_pipeline("en_full_10", None, shuffle_flag=False)
    trainer.german_pipeline("de_full_10", None, shuffle_flag=False)

    trainer.english_1k_train_pipeline("en_1k_01", None, shuffle_flag=False)
    trainer.german_1k_train_pipeline("de_1k_01", None, shuffle_flag=False)


if __name__ == "__main__":
    main()
